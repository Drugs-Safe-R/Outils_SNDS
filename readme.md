# Outils SNDS

*Bienvenue sur le projet Outils_SNDS. Il contient des macros-commandes SAS permettant le traitement des données du SNDS.*  
*Le but est de faciliter l'utilisation de ces données aux utilisateurs novices comme expérimentés.*

## Présentation

-   L'ensemble des macros sont en **langage SAS**.  

-   Ces macros sont à utiliser sur SAS du **portail SNDS**.  

-   Pour pouvoir accéder au portail de données SNDS et utiliser ces macros, il faut avoir suivi les formations autorisant l'accès au SNDS.   

-   Le projet est divisé en deux dossiers : **Extraction** et **Référentiel**.  
Dans le premier se trouve des macros permettant d'extraire des données, dans le DCIR et les PMSI, concernant les médicaments (ATC, CIP), les diagnostics des séjours hospitaliers (CIM10), les ALD, les actes médicaux (CCAM) et les dispositifs médicaux (LPP).  
  Dans le second se trouve des macros permettant de constituer un référentiel de patients avec un identifiant unique, éliminant ceux avec des incohérences concernant leurs données socio-démographiques et donnant un historique de leurs affiliations annuelles sur la période d'étude.  

-   Pour les utiliser, récupérer les macros dont vous avez besoin. Les *faire tourner* sur **SAS du portail SNDS**.  
Puis dans un second temps les *appeler* en suivant l'exemple à la fin du paragraphe de présentation de chacune d'elle.

## Références utiles

-   *Information complémentaire pour utiliser les macros Référentiel* : Les macros référentiel nécessite l'identification au préalable d'une population d'intérêt (à l'aide d'une macro extraction par exemple ou par vos propres algorithmes d'extraction).

## Auteurs

-   **_Centre DRUGS SAFE<sup>R</sup>_**. 

## Licence

Ce projet, les macros et documentations sont sous licence **Apache 2.0**.
