
/*****************************************************************************************************************
*	Macro  : ref_nir_aff															    						 *
*	Date   : 19/03/2024																							 *
*	**************************************************************************************************************
*	Apache License : Copyright 2023, AHeaD Team Bordeaux Population Health U1219								 *
*	**************************************************************************************************************
*	Mises a jour : (le cas echeant : preciser date et objet de la MAJ)											 *
*			19/03/2024 - JB : ajout de la creation d'une table finale											 *
*																												 *
*	**************************************************************************************************************
*	Description : Cette macro permet d'identifier pour chaque annee et pour chaque sujet les informations		 *
*		concernant les donnees d'affiliation aux regimes de securite sociale. En plus de connaitre pour chaque	 *
*		BEN_IDT_ANO, le fait que les sujets soient sous CMU et/ou AME au moins une fois dans l'annee, ce		 *
*		programme identifie le regime d'affilition. Au moins un jour d'affiliation est considere				 *
*		par la variable "Regime_Annee", de maniere priorisante RG > SLM > RSI > MSA > Autres > Non Affilie		 *
*	**************************************************************************************************************
*	Parametres :																								 *
*		lib_prj 	: nom de la bibliotheque ou sera rangee la table de sortie									 *
*		ann_deb		: Annee a laquelle on veut avoir le debut des informations d'affiliation de notre population *
*		ann_fin		: Annee a laquelle on veut avoir la fin des informations d'affiliation de notre population	 *
*		dat_fix		: Date, par defaut celle du lancement du programme, permettant de reiterer les memes donnees *
*					a n'importe quel moment. Si l'annee de fin indiquee correspond a une annee dont la table	 *
*					Extractions_PatientsTR de la librairie Consopat n'est pas existante, nous recreons cette	 *
*					table a partir des donnees du DCIR dont la date de requetage borne l'information par rapport *
*					a la date de flux. Si cette date n'est pas renseignee, la date (dat_fix) est la date du jour *
*					de l'execution du programme. Cette option doit etre renseignee de la forme suivante :		 *
*					03/09/2023, sans guillemets																	 *
*	**************************************************************************************************************
*	Resultat : table contenant les variables suivantes :														 *
*		BEN_NIR_OPT : Nouvel identifiant cree, anonymise														 *
*		Regime_20AA : Affiliation au moins un jour au regime de securite sociale								 *	
*						0 - Au moins un jour sous le regime general											 	 *
*						1 - Au moins un jour sous SLM (sans etre sous RG)										 *
*						2 - Au moins un jour sous RSI (sans etre RG, SLM)									 	 *
*						3 - Au moins un jour sous MSA (sans etre RG, SLM, RSI)									 *
*						4 - Au moins un jour sous un autre regime (sans etre RG, SLM, RSI, MSA)				 	 *
*						. - Non affilie durant toute l'annee												 	 *
*		BEN_CMU_20AA : Indicateur binaire indique avoir la CMUc dans l'annee (1 - Oui)							 *
*		BEN_AME_20AA : Indicateur binaire indique avoir l'AME dans l'annee (1 - Oui)						 	 *
******************************************************************************************************************
* 	Exemple	:																									 *
* 	%ref_nir_aff(lib_prj=LIBABCD, ann_deb=2015, ann_fin=2022, dat_fix=03/05/2023);				 				 *
*	Cet exemple permet d'extraire toutes les donnees annuelles d'afilliation au regime de la securite sociale	 *
*	de la population issue de votre table REF_NIR_CLE, entre 2015 et 2022. La requete ne prend que les 			 *
*	informations du DCIR au 03/05/2023 maximum pour determiner les donnees d'affiliaiton sur l'annee 2022 (la	 *
*	table EXTRACTIONS_PATIENTS2022TR n'existant pas au moment de la creation de cet exemple						 *
*****************************************************************************************************************/

%macro ref_nir_aff(lib_prj, ann_deb, ann_fin, dat_fix);
		
/* Definition automatique annee consopat non presente */
	%let exist = 1;
	%let aa = 2006;
	%do %while (&exist. = 1);
		%if %sysfunc(exist(consopat.extraction_patients&aa.TR, VIEW))=1 %then %do;
			%let exist = 1;
			%let aa = %eval(&aa.+1);
		%end;
		%else %do;
			%let exist = 0;
		%end;
	%end;
	%let ann_consopat = %eval(&aa.);

	/*%do aaaa = %sysfunc(max(&ann_dbt., 2006)) %to %sysfunc(min(%sysfunc(%eval(&ann_fin. + 2)), &ann_act.));*/
%if &ann_fin. GE &ann_consopat. %then %do;

%macro checkDrop(myData);
	%if %SYSFUNC(exist(orauser.&myData)) %then
		%do;

			proc sql;
				%connectora;
				EXECUTE(
					drop table &myData
						)
					BY ORACLE;
			quit;

		%end;
%mend;

/*Macro multi_like*/
%macro multi_like(var, lst_cod, aim, stp);
	%local i val ope;
	%let i = 1;
	%let val =;
	%let ope =;

	%do %until (%superq(val) =);
		%let val = %qscan(&lst_cod., &i., %str( ));

		%if %sysfunc(verify(&val., '0123456789')) > 0 %then
			%do;
				%if &aim. = INC %then
					%do;
						%if &stp. = PROC %then
							%do;
								%if %superq(val) > 0 %then
									%do;
										%cmpres(%str(&ope. %upcase(&var.) like %'%upcase(&val.)%%%'))
									%end;

								%put %str(&ope. %upcase(&var.) like %'%upcase(%superq(val))%%%');
							%end;

						%if &stp. = DATA %then
							%do;
								%if %superq(val) > 0 %then
									%do;
										%cmpres(%str(&ope. %upcase(&var.) =: %'%upcase(&val.)%'))
									%end;

								%put %str(&ope. %upcase(&var.) =: %'%upcase(%superq(val))%');
							%end;

						%let ope = or;
					%end;

				%if &aim. = EXC %then
					%do;
						%if &stp. = PROC %then
							%do;
								%if %superq(val) > 0 %then
									%do;
										%cmpres(%str(&ope. %upcase(&var.) not like %'%upcase(&val.)%%%'))
									%end;

								%put %str(&ope. %upcase(&var.) not like %'%upcase(%superq(val))%%%');
							%end;

						%if &stp. = DATA %then
							%do;
								%if %superq(val) > 0 %then
									%do;
										%cmpres(%str(&ope. %upcase(&var.) ^=: %'%upcase(&val.)%'))
									%end;

								%put %str(&ope. %upcase(&var.) ^=: %'%upcase(%superq(val))%');
							%end;

						%let ope = and;
					%end;
			%end;

		%if %sysfunc(verify(&val., '0123456789')) = 0 %then
			%do;
				%if &aim. = INC %then
					%do;
						%if %superq(val) > 0 %then
							%do;
								%cmpres(%str(&ope. (&var.) = (&val.)))
							%end;

						%put %str(&ope. (&var.) = (%superq(val)));
						%let ope = or;
					%end;

				%if &aim. = EXC %then
					%do;
						%if %superq(val) > 0 %then
							%do;
								%cmpres(%str(&ope. (&var.) ^= (&val.)))
							%end;

						%put %str(&ope. (&var.) ^= (%superq(val)));
						%let ope = and;
					%end;
			%end;

		%let i = %eval(&i. + 1);
	%end;
%mend;

%macro copie_tab_pop(lib_prj);
	/*copie ref_nir_cle vers orauser*/
	%checkDrop(REF_NIR_CLE);

	proc sql;
		create table orauser.REF_NIR_CLE (BULKLOAD=yes BL_DATAFILE="%sysfunc(pathname(work))/data.dat" BL_DELETE_DATAFILE=yes) 
			as select * from &lib_prj..REF_NIR_CLE;
	quit;

%mend;

%macro xtr_pat_sup(lib_prj, dat_fix);
	options dbsliceparm=(all, 4);

	%let ann_act = %sysfunc(year(%sysfunc(today())));
	%let dbt = '01/01/&ann_consopat';
	%let date_dbt=%sysfunc(compress(&dbt, '/'));
	%let diff =	%eval(&ann_act-1);


	%checkDrop(DT_PRS_&ann_consopat.);
	%copie_tab_pop(&lib_prj.);

	%if &ann_consopat = &diff %then %do;
		%let limit = %eval(12+%sysfunc(month(%sysfunc(today()))));
	%end;
	%else %do;
		%let limit = 38;
	%end;


	%do i = 0 %to &limit.;
		%let flxDbt=%sysfunc(intnx(MONTH,%sysfunc(inputn(%sysfunc(mdy(1,1,&ann_consopat.),ddmmyyn8.),ddmmyy8.)),&i),ddmmyyn8.);
		%let flxFin=%sysfunc(intnx(MONTH,%sysfunc(inputn(%sysfunc(mdy(1,1,&ann_consopat.),ddmmyyn8.),ddmmyy8.)),%eval(&i.+1)),ddmmyyn8.);

		proc sql;
			%connectora;
			EXECUTE(
				%if %sysfunc(exist(orauser.DT_PRS_&ann_consopat.))=0 %then

				%do;
					create table DT_PRS_&ann_consopat. as
				%end;
				%else
					%do;
						insert into DT_PRS_&ann_consopat.
					%end;

			(select distinct t_prs.BEN_NIR_PSA,
							 t_pop.BEN_NIR_OPT,
					t_prs.ORG_AFF_BEN,
					t_prs.ORG_CLE_NEW,
					case 
						when substr(ORG_AFF_BEN,1,3)='01C' then 'RG'
						when substr(ORG_AFF_BEN,1,3)='02A' then 'MSA'
						when substr(ORG_AFF_BEN,1,3)='03A' then 'RSI'
						when substr(ORG_AFF_BEN,1,3)='04A' then 'SNCF'
						when substr(ORG_AFF_BEN,1,3)='05A' then 'RATP'
						when substr(ORG_AFF_BEN,1,3)='06A' then 'ENIM'
						when substr(ORG_AFF_BEN,1,3)='07A' then 'mines'
						when substr(ORG_AFF_BEN,1,3)='08A' then 'CNMSS'
						when substr(ORG_AFF_BEN,1,3)='10A' then 'CRPCEN'
						when substr(ORG_AFF_BEN,1,3)='90A' then 'CAVIMAC'
						when (substr(ORG_AFF_BEN,1,3)='01M' or 
								(substr(ORG_AFF_BEN,1,1)='9' and substr(ORG_AFF_BEN,3,1)='C')) then 'SLM'
					end as regime, 
					t_prs.BEN_RES_DPT,
					t_prs.BEN_RES_COM,
					t_prs.BEN_CMU_TOP,
					case when t_prs.BEN_CMU_CAT = '5' then 1
						else 0 
					end	as BEN_AME_TOP,
					t_prs.BEN_RNG_GEM,
					t_prs.BEN_DCD_DTE,
					t_prs.BEN_DCD_AME,
					t_prs.BEN_SEX_COD,
					t_prs.BEN_NAI_ANN,
					t_prs.EXE_SOI_DTD,
					t_prs.EXE_SOI_DTF,
					t_prs.FLX_DIS_DTD

			from REF_NIR_CLE t_pop inner join ER_PRS_F t_prs on t_pop.BEN_NIR_PSA = t_prs.BEN_NIR_PSA 
				where
					(t_prs.EXE_SOI_DTD >= to_date(%str(%'&date_dbt.%'), 'ddmmyyyy')) and 
					(t_prs.FLX_DIS_DTD >= to_date(%str(%'&flxDbt.%'), 'ddmmyyyy') and t_prs.FLX_DIS_DTD < to_date(%str(%'&flxFin.%'), 'ddmmyyyy'))
				)
			)
			BY ORACLE;
				quit;

	%end;

	/* copie table PRS dans la work */
	%if &dat_fix. ne %then %do;
		%let fix_dat = &dat_fix.;
	%end;
	%if &dat_fix. =  %then %do;
		%let fix_dat = %sysfunc(today(), ddmmyy10.);
	%end;
	%let d=%substr(&fix_dat.,1,2);
	%let m=%substr(&fix_dat.,4,2);
	%let y=%substr(&fix_dat.,7,4);

	%let fin = 31DEC&ann_consopat.;


	proc sql;
	create table DT_PRS_&ann_consopat.
	as select distinct *
	from orauser.DT_PRS_&ann_consopat.;
	quit;


	/*Creation de la table EXTRACTION_PATIENTS dans la librairie projet*/
	proc sql;
	create table EXTRACTION_PATIENTS&ann_consopat.TR as
	select distinct prs.BEN_NIR_PSA,
	                    prs.BEN_NIR_OPT,
	                    prs.ORG_AFF_BEN,
	                    prs.ORG_CLE_NEW,
	                    max(datepart(prs.exe_soi_dtd)) as max_dt format ddmmyy10.,
	                    prs.regime,
	                    prs.BEN_RES_DPT,
	                    prs.BEN_RES_COM,
	                    prs.BEN_CMU_TOP,
			    max(prs.BEN_AME_TOP) as BEN_AME_TOP,
	                    prs.BEN_RNG_GEM,
	                    prs.BEN_DCD_DTE,
	                    prs.BEN_DCD_AME,
	                    prs.BEN_SEX_COD,
	                    prs.BEN_NAI_ANN
	            from DT_PRS_&ann_consopat. prs
				where datepart(flx_dis_dtd)<=mdy(&m.,&d.,&y.)	
	            group by prs.BEN_NIR_OPT, prs.regime;
	    quit;

	    proc sql;
	        create table &lib_prj..EXTRACTION_PATIENTS&ann_consopat.TR as
	            select distinct  BEN_NIR_PSA,
	                    BEN_NIR_OPT,
	                    ORG_AFF_BEN,
	                    ORG_CLE_NEW,
	                    regime,
						case when max_dt > "&fin."d then "&fin."d
                             when max_dt <= "&fin."d then max_dt
                        end as DDS format ddmmyy10.,
	                    BEN_RES_DPT,
	                    BEN_RES_COM,
	                    BEN_CMU_TOP,
						BEN_AME_TOP,
	                    BEN_RNG_GEM,
	                    BEN_DCD_DTE,
	                    BEN_DCD_AME,
	                    BEN_SEX_COD,
	                    BEN_NAI_ANN
	            from EXTRACTION_PATIENTS&ann_consopat.TR;
	    quit;


	/* suppression tables temporaires*/
	proc sql;
		drop table work.EXTRACTION_PATIENTS&ann_consopat.TR;
	quit;
	proc sql;
		drop table work.DT_PRS_&ann_consopat.;
	quit;
	%checkDrop(DT_PRS_&ann_consopat.);
	%checkDrop(REF_NIR_CLE);
%mend;

	%xtr_pat_sup(&lib_prj.,&dat_fix.)
%end;

	
		/* Recuperation des donnees dans les tables CONSOPAT */
	
%let deb = %sysfunc(max(%eval(&ann_deb.), 2006));
%let fin = %sysfunc(min(%eval(&ann_fin.), &ann_consopat.));

%do aaaa = &deb. %to &fin.;

	%if &aaaa. < &ann_consopat. %then %do;
		%let tab_extract = CONSOPAT.EXTRACTION_PATIENTS&aaaa.TR;
	%end;
	%if &aaaa. = &ann_consopat. %then %do;
		%let tab_extract = &lib_prj..EXTRACTION_PATIENTS&aaaa.TR;
	%end;

	proc sql;
		create table REF_NIR_PROT_&aaaa. as
		select t1.BEN_NIR_OPT,
			   max(t2.BEN_CMU_TOP) as BEN_CMU_&aaaa.,
			   max(t2.BEN_AME_TOP) as BEN_AME_&aaaa.
		from &lib_prj..REF_NIR_CLE t1 left join &tab_extract. t2 on t1.BEN_NIR_PSA = t2.BEN_NIR_PSA
		group by t1.BEN_NIR_OPT;
	quit;

	proc sql;
		create table REF_NIR_REG_&aaaa. as
		select distinct t1.BEN_NIR_OPT,
				min(case
					when regime in ('RG') then 0
					when regime in ('SLM') then 1
				   	when regime in ('RSI') then 2
				   	when regime in ('MSA') then 3
				   	when regime not in ('RG','SLM','RSI','MSA') and regime ne '' then 4
				   	else .
				end) as Regime_&aaaa.
		from &lib_prj..REF_NIR_CLE t1 left join &tab_extract. t2 on t1.BEN_NIR_PSA = t2.BEN_NIR_PSA
		group by t1.BEN_NIR_OPT;
	quit;

	proc sql;
		create table REF_NIR_DAT_&aaaa. as
		select distinct t1.BEN_NIR_OPT,
				max(DDS) as DDS_&aaaa.
		from &lib_prj..REF_NIR_CLE t1 left join &tab_extract. t2 on t1.BEN_NIR_PSA = t2.BEN_NIR_PSA
		group by t1.BEN_NIR_OPT;
	quit;
%end;

	proc sql;
		create table &lib_prj..REF_NIR_AFF as
		select distinct %do aaaa = &deb. %to &fin.;
							Regime_&aaaa.,
						%end;
						%do aaaa = &deb. %to &fin.;
							DDS_&aaaa.,
						%end;
						%do aaaa = &deb. %to &fin.;
							BEN_CMU_&aaaa.,
						%end;
						%do aaaa = &deb. %to &fin.;
							BEN_AME_&aaaa.,
						%end;
						t1.BEN_NIR_OPT	
		from &lib_prj..REF_NIR_CLE t1
			%do aaaa = &deb. %to &fin.;
				left join REF_NIR_PROT_&aaaa. t1&aaaa. on t1.BEN_NIR_OPT = t1&aaaa..BEN_NIR_OPT
				left join REF_NIR_REG_&aaaa. t2&aaaa. on t1.BEN_NIR_OPT = t2&aaaa..BEN_NIR_OPT
				left join REF_NIR_DAT_&aaaa. t3&aaaa. on t1.BEN_NIR_OPT = t3&aaaa..BEN_NIR_OPT
			%end;;
	quit;

%if &dat_fix. = %then %do;
	%put WARNING: Vous n avez pas complete la variable dat_fix lors de l execution de la requete. Elle a pris par defaut la date au cours de laquelle vous avez execute le programme. Si vous relancez ce programme sans indiquer de date, les resultats peuvent varier;
%end;
%if &ann_fin. GE &ann_consopat. %then %do;
	%put WARNING: La table EXTRACTION_PATIENTS dans CONSOPAT est inexistante au moment de l execution du programme. Nous avons recree une table similaire a partir des donnees prestations sur ER_PRS_F, selon la date renseignee en option (ou date du jour d execution dans le cas echeant). Il faut tenir compte que la nouvelle table EXTRACTION_PATIENTS creee dans votre librairie peut etre imprecise, et varier selon la date fixee;
%end;
%if &ann_fin. > &ann_consopat. %then %do;
	%put WARNING: L annee de fin renseignee dans votre requete est trop ulterieure aux donnees disponibles dans le SNDS. Nous avons stoppe la requete a l annee &ann_consopat.;
%end;

/* creation table finale */
proc sql;
		create table &lib_prj..REF_NIR_TOT as
		select *
		from &lib_prj..REF_NIR_DEM t1
		left join &lib_prj..ref_nir_aff t2 on t1.BEN_NIR_OPT = t2.BEN_NIR_OPT
		left join &lib_prj..ref_nir_cle t3 on t1.BEN_NIR_OPT = t3.BEN_NIR_OPT
		left join &lib_prj..ref_nir_dc t4 on t1.BEN_NIR_OPT = t4.BEN_NIR_OPT
			where 
			%do aaaa = &deb. %to &fin.-1;
				 regime_&aaaa. is not null or
			%end; 
			regime_&fin. is not null;
quit;

%mend;
