/****************************************************************************************************************
*	Macro  : ref_ir_ben																    						*
*	Date   : 19/03/2024																							*
*	*************************************************************************************************************
*	Apache License : Copyright 2023, AHeaD Team Bordeaux Population Health U1219								*
*	*************************************************************************************************************
*	Mises a jour : (le cas echeant : preciser date et objet de la MAJ)											*
* 			19/03/2024 - JB : modification du filtre BEN_CDI_NIR												*	
*																												*
*	*************************************************************************************************************
*	Description : Cette macro permet d'extraire tous les sujets identifies dans le SNDS. Un filtre est propose	*
*			afin de ne recuperer que les NIR codes comme normaux, provisoires, fictifs ou tout venant			*
*	*************************************************************************************************************
*	Parametres :																								*
*		lib_prj 	: nom de la bibliotheque où sera rangee la table de sortie									*
*		opt_nir 	: Permet de selectionner les NIR selon leur modalite de codage								*
*						1 : NIR Normal (00)																		*			
*						2 : NIR Normal (00)																		*
*							NIR D UN MIGRANT PROVISOIRE PASSAGE (commence par 5 ou 6) (03) 						*
*							NIR D UN ASSURE PROVISOIRE (commence par 7 ou 8) (04)								*
*						3 : NIR Normal (00)																		*				
*							NIR FICTIF IVG (01)																	*			
*							NIR D UN MIGRANT PROVISOIRE PASSAGE (commence par 5 ou 6) (03) 						*
*							NIR D UN ASSURE PROVISOIRE (commence par 7 ou 8) (04)								*
*							NIR FICTIF IVG DES MINEURES SANS CONSENTEMENT PARENTAL (06)							*
*						Sinon : NIR Normal (00)																	*			
*								NIR FICTIF IVG (01)																*			
*								NIR D UN MIGRANT PROVISOIRE PASSAGE (commence par 5 ou 6) (03) 					*
*								NIR D UN ASSURE PROVISOIRE (commence par 7 ou 8) (04)							*	
*								NIR FICTIF IST (05)																*		
*								NIR FICTIF IVG DES MINEURES SANS CONSENTEMENT PARENTAL (06)						*
*								NIR FICTIF AUTRE (08)															*					
*								NIR FICTIF DES AIDES A LA TELETRANSMISSION (09)									*
*								NIR FICTIF CONTRACEPTION DES MINEURES (11)										*	
*								ALD MINEUR (12)																	*
*								NIR MATERNITE PS (13)															*		
*								NIR PATERNITE PS (14)															*				
*								NIR ADOPTION PS (15)															*		
*	*************************************************************************************************************
*	Resultat : table contenant les variables suivantes :														*
*		BEN_IDT_ANO : Identifiant beneficiaire anonymise														*
*		BEN_NIR_PSA : Identifiant anonyme du patient dans le SNIIRAM											*
*		BEN_RNG_GEM : Rang de naissance du beneficiaire															*
*		BEN_NIR_ANO : NIR pseudonymise du beneficiaire															*
*		ASS_NIR_ANO : Matricule anonymise de l'ouvreur de droits												*
*		BEN_CDI_NIR : Code d'identification du NIR																*
*		BEB_SEX_COD : Sexe du sujet																				*
*		BEN_NAI_ANN : Annee de naissance du sujet																*
*		BEN_NAI_MOI : Mois de naissance du sujet																*
*		BEN_DCD_DTE : Date de deces du sujet																	*	
*		SOURCE		: Valeur binaire indiquant la table d'origine du sujet IR_BEN_R (1) ou IR_BEN_R_ARC (2)		*
*****************************************************************************************************************
* 	Exemple	:																									*
* 	%ref_ir_ben(lib_prj = LIBABCD, opt_nir=2);																	*
*	Cet exemple extrait tous les identifiants consideres comme normaux ou provisoires BEN_IDT_ANO avec leurs 	*
*	correspondances du DCIR, avec une ecriture de la table dans la libraire LIBABCD								*
****************************************************************************************************************/


%macro ref_ir_ben(lib_prj, opt_nir);
		%if &opt_nir. = 1 %then %do; /* NIR Normal */
			%let nir_cod = ('00');
		%end;
		%if &opt_nir. = 2 %then %do; /* NIR Normal et Provisoires */
			%let nir_cod = ('00','03','04');
		%end;
		%if &opt_nir. = 3 %then %do; /* NIR Normal et Provisoires et IVG */
			%let nir_cod = ('00','01','03','04','06');
		%end;
		%if &opt_nir. = 4 %then %do; /* Tout type de NIR hors valeur inconnue et sans objet */
			%let nir_cod = ('00','01','02','03','04','05','06','08','09','11','12','13','14','15');
		%end;

	/* 1 - Initialisation */
	options dbidirectexec dbsliceparm=(all, 4);
	proc sql;
		drop table &lib_prj..REF_IR_BEN;
	quit;

	/* 2 - Concatenation des tables IR_BEN_R et IR_BEN_R_ARC */
	proc sql;
		create table &lib_prj..REF_IR_BEN as
		select distinct BEN_IDT_ANO,
						BEN_NIR_PSA,
						BEN_RNG_GEM,
						BEN_NIR_ANO,
						ASS_NIR_ANO,
						BEN_SEX_COD,
						BEN_NAI_ANN,
						BEN_NAI_MOI,
						BEN_CDI_NIR,
						BEN_DCD_DTE,
						1 as SOURCE
		from ORAVUE.IR_BEN_R
		union
		select distinct t1.BEN_IDT_ANO,
						t1.BEN_NIR_PSA,
						t1.BEN_RNG_GEM,
						t1.BEN_NIR_ANO,
						t1.ASS_NIR_ANO,
						t1.BEN_SEX_COD,
						t1.BEN_NAI_ANN,
						t1.BEN_NAI_MOI,
						t1.BEN_CDI_NIR,
						t1.BEN_DCD_DTE,
						2 as SOURCE
		from ORAVUE.IR_BEN_R_ARC t1 left join ORAVUE.IR_BEN_R t2 on t1.BEN_NIR_PSA = t2.BEN_NIR_PSA and t1.BEN_RNG_GEM = t2.BEN_RNG_GEM
		where t2.BEN_NIR_PSA is null;
quit;

%put &nir_cod;

	/* 3 – Filtre sur la modalite du NIR 
Sont conservés, les codes identifiants manquants (98% des pseudoNIR dans IR_BEN_R_ARC), sans objets (‘NN’ représentant 0,006% des pseudoNIR ; 31% des pseudo-NIR ‘NN’ sont associés à un BEN_NIR_ANO ; 15% des pseudo-NIR ‘NN’ sont associés au régime général) et ‘XX’ (‘XX’ représentant 0,004% des pseudoNIR ; 100% des pseudo-NIR ‘XX’ sont associés à un BEN_NIR_ANO ; 97% des pseudo-NIR ‘XX’ sont associés au régime général).*/

proc sql;
	create table &lib_prj..ref_ir_ben as
	select *
	from &lib_prj..ref_ir_ben
	where BEN_CDI_NIR in %scan(&nir_cod.,1,"/") or (SOURCE = 2 and BEN_CDI_NIR is missing) or (BEN_CDI_NIR in ('NN', 'XX'));
quit;

%mend;
