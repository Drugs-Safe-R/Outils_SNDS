/****************************************************************************************************************
*	Macro  : ref_nir_opt															    						*
*	Date   : 24/02/2025																							*
*	*************************************************************************************************************
*	Apache License : Copyright 2023, AHeaD Team Bordeaux Population Health U1219								*
*	*************************************************************************************************************
*	Mises a jour : (le cas echeant : preciser date et objet de la MAJ)											*
*			19/03/2024 - JB : Modification du filtre sur les jumeaux, sur les pseudoNIR aberrants et le 		*
*							  diagramme de flux																	*
*																												*
*	*************************************************************************************************************
*	Description : Cette macro permet d'identifier toutes les combinaisons de pseudo-NIR par sujets en utilisant *
*			l'identifiant BEN_IDT_ANO. La macro permet d'exclure la présence de jumeaux (impossible a 			*
*			distinguer dans le PMSI et d'exclure les sujets avec un nombre de pseudo-NIR aberrants (>10).		*
*	*************************************************************************************************************
*	Parametres :																								*
*		lib_prj 	: nom de la bibliotheque ou sera rangee la table de sortie									*
*		opt_diag 	: Permet de creer le diagramme de flux, valeur - 1 oui										*	
*	*************************************************************************************************************
*	Resultat : table contenant les variables suivantes :														*
*		BEN_NIR_OPT : Nouvel identifiant cree, anonymise														*
*		BEN_IDT_ANO : Identifiant beneficiaire anonymise														*
*		BEN_NIR_PSA : Identifiant anonyme du patient dans le SNIIRAM											*	
*		BEN_RNG_GEM : Rang de naissance du beneficiaire															*
*		BEN_NIR_ANO : NIR pseudonymise du beneficiaire															*
*		ASS_NIR_ANO : Matricule anonymise de l'ouvreur de droits												*
*		BEN_CDI_NIR : Code d'identification du NIR																*
*****************************************************************************************************************
* 	Exemple	:																									*
* 	%ref_nir_opt(lib_prj = LIBABCD, opt_diag=);																	*
*	Cet exemple identifie les combinaisons indiques pour un meme identifiant BEN_IDT_ANO, avec une ecriture de	*
*	la table de sortie dans la libraire LIBABCD. L'option opt_diag n'etant pas complete, le diagramme de flux	*
*	ne sera pas cree.																							*
****************************************************************************************************************/


%macro ref_nir_opt(lib_prj, opt_diag);

	/* 1 - Initialisation */
	options dbidirectexec dbsliceparm=(all, 4);
	proc sql;
		drop table &lib_prj..REF_NIR_OPT_2;
		drop table &lib_prj..REF_NIR_OPT_3_1;
		drop table &lib_prj..REF_NIR_OPT_3_2;
		drop table &lib_prj..REF_NIR_OPT_4_1;
		drop table &lib_prj..REF_NIR_OPT_4_2;
		drop table &lib_prj..REF_NIR_OPT_4_3;
		drop table &lib_prj..REF_NIR_OPT_5;
		drop table &lib_prj..RES;
		drop table &lib_prj..REF_NIR_OPT;
	quit;

	/* 2 – Creation REF_NIR_OPT */
	proc sql;
		create table &lib_prj..REF_NIR_OPT_2 as
		select distinct BEN_NIR_PSA,
						BEN_RNG_GEM,
						BEN_NIR_ANO,
						BEN_IDT_ANO,
						BEN_IDT_ANO as BEN_NIR_OPT,
						ASS_NIR_ANO,
						BEN_SEX_COD,
						BEN_NAI_ANN,
						BEN_CDI_NIR
		from &lib_prj..REF_IR_BEN;
	quit;

	/* 3 - Filtre sur les beneficiaires sans jumeaux/jumelles de même sexe */
	proc sql;
		create table &lib_prj..REF_NIR_OPT_3_1 as
		select distinct BEN_IDT_ANO
		from &lib_prj..REF_NIR_OPT_2
		group by BEN_NIR_PSA
		having count(distinct BEN_IDT_ANO)> 1;
	quit;

	proc sql;
		create table &lib_prj..REF_NIR_OPT_3_2 as
		select distinct t1.BEN_NIR_OPT,
						t1.BEN_IDT_ANO,
						t1.BEN_NIR_PSA,
						t1.BEN_RNG_GEM,
						t1.BEN_NIR_ANO,
						t1.ASS_NIR_ANO,
						t1.BEN_CDI_NIR
		from &lib_prj..REF_NIR_OPT_2 t1
		where BEN_IDT_ANO not in (select BEN_IDT_ANO from &lib_prj..REF_NIR_OPT_3_1)
		order by BEN_NIR_OPT;
	quit;

	/* 4 - Anonymisation de BEN_NIR_OPT */
	data &lib_prj..REF_NIR_OPT_4_1;
		set &lib_prj..REF_NIR_OPT_3_2;
		by BEN_NIR_OPT;
		LAG_NIR_OPT = lag(BEN_NIR_OPT);
		if _N_ = 1 then LAG_NIR_OPT = BEN_NIR_OPT;
	run;
	data &lib_prj..REF_NIR_OPT_4_2;
		set &lib_prj..REF_NIR_OPT_4_1;
		retain i 1;
		if BEN_NIR_OPT ^= LAG_NIR_OPT then i = i + 1;
		NIR_OPT_ANO = i;
		drop BEN_NIR_OPT LAG_NIR_OPT i;
	run;
	data &lib_prj..REF_NIR_OPT_4_3;
		set &lib_prj..REF_NIR_OPT_4_2(rename=(NIR_OPT_ANO = BEN_NIR_OPT));
	run;

	/* 5 - Filtre sur les BEN_NIR_OPT associes < 10 BEN_NIR_PSA-BEN_RNG_GEM */
	proc sql;
		create table &lib_prj..REF_NIR_OPT_5 as
		select distinct BEN_NIR_OPT
		from &lib_prj..REF_NIR_OPT_4_3
		group by BEN_NIR_OPT
		having count(*) < 10;

		create table &lib_prj..REF_NIR_OPT as
		select distinct t1.BEN_NIR_OPT,
						t2.BEN_IDT_ANO,
						t2.BEN_NIR_PSA,
						t2.BEN_RNG_GEM,
						t2.BEN_NIR_ANO,
						t2.ASS_NIR_ANO,
						t2.BEN_CDI_NIR
		from &lib_prj..REF_NIR_OPT_5 t1 inner join &lib_prj..REF_NIR_OPT_4_3 t2 on t1.BEN_NIR_OPT = t2.BEN_NIR_OPT;
	quit;

	%if &opt_diag. = 1 %then %do;
	/* 6 - Rsum quantitatif (flowchart) de la constitution de REF_NIR_OPT */
	proc format;
		value I
		1 = 'Pseudo-NIR issus de IR_BEN_R et IR_BEN_R_ARC'
		2 = 'Personnes issues de IR_BEN_R et IR_BEN_ARC'
		3 = 'Pseudo-NIR sniiram sans jumeaux/jumelles'
		4 = 'Personnes sans jumeaux/jumelles'
		5 = 'Personnes incluses dans REF_NIR_OPT après nettoyage des incohérences '
		;
	run;
	
	proc sql;
		create table &lib_prj..res1 as
		select distinct BEN_NIR_PSA, BEN_RNG_GEM
		from &lib_prj..REF_NIR_OPT_2;
	quit;
	data &lib_prj..res1b;I=1;N=&sqlobs.;run;
	proc sql;
		create table &lib_prj..res2 as
		select distinct BEN_IDT_ANO
		from &lib_prj..REF_NIR_OPT_2;
	quit;
	data &lib_prj..res2b;I=2;N=&sqlobs.;run;
	proc sql;
		create table &lib_prj..res3 as
		select distinct BEN_NIR_PSA, BEN_RNG_GEM
		from &lib_prj..REF_NIR_OPT_4_3;
	quit;
	data &lib_prj..res3b;I=3;N=&sqlobs.;run;
	proc sql;
		create table &lib_prj..res4 as
		select distinct BEN_NIR_OPT
		from &lib_prj..REF_NIR_OPT_4_3;
	quit;
	data &lib_prj..res4b;I=4;N=&sqlobs.;run;
	proc sql;
		create table &lib_prj..res5 as
		select distinct BEN_NIR_OPT
		from &lib_prj..REF_NIR_OPT;
	quit;
	data &lib_prj..res5b;I=5;N=&sqlobs.;run;
	data &lib_prj..res;
	set &lib_prj..res1b &lib_prj..res2b &lib_prj..res3b &lib_prj..res4b &lib_prj..res5b;
	run;

	proc datasets library=&lib_prj. nolist nodetails;
    delete res1 res1b res2 res2b res3 res3b res4 res4b res5 res5b ;
	quit;
	proc report data=&lib_prj..RES nowd;
		column I N;
		define I / display right '' order=data format=I.;
		define N / display right 'Effectif';
	run;
	%end;
%mend;


