# Référentiel
Version du 19/03/2024

_Le référentiel est sous licence Apache 2.0._


## 1. Récupération de tous les identifiants disponibles dans le SNDS

Création de la table REF_IR_BEN regroupant tous les identifiants issus des tables IR_BEN_R et IR_BEN_R_ARC

Filtre sur le code d’identification du NIR
•	NIR Normal ('00')
•	NIR Normal et Provisoires ('00','03','04')
•	NIR Normal et Provisoires et IVG ('00','01','03','04','06')
•	Tout type de NIR hors valeur inconnue et sans objet ('00','01','02','03','04','05','06', '08','09','11','12','13','14','15')

Sont conservés, les codes identifiants manquants (98% des pseudoNIR dans IR_BEN_R_ARC), sans objets (‘NN’ représentant 0,006% des pseudoNIR ; 31% des pseudo-NIR ‘NN’ sont associés à un BEN_NIR_ANO ; 15% des pseudo-NIR ‘NN’ sont associés au régime général) et ‘XX’ (‘XX’ représentant 0,004% des pseudoNIR ; 100% des pseudo-NIR ‘XX’ sont associés à un BEN_NIR_ANO ; 97% des pseudo-NIR ‘XX’ sont associés au régime général).

## 2. Association de tous les couples d’identifiants sous un identifiant unique créé pour l’occasion

Création de la table REF_NIR_OPT pour identifier les identifiants en doublon et changements de NIR tout au long du suivi du DCIR. Un identifiant unique BEN_NIR_OPT est créé pour l’occasion.

Filtre sur les bénéficiaires sans jumeaux/jumelles (pour appariement PMSI) :
Pour le RG y compris SLM le rang gémellaire permet de distinguer les naissances gémellaires de même sexe. Pour tous les autres régimes, BEN_RNG_GEM permet de différencier les bénéficiaires rattachés au même ouvreur de droit. 
Exclusion des BEN_IDT_ANO qui ont plusieurs rangs gémellaires et plusieurs BEN_NIR_ANO pour un même BEN_NIR_PSA (0,95% des BEN_IDT_ANO).

Filtre sur le nombre aberrant de BEN_NIR_PSA relié à un BEN_IDT_ANO (≥ 10 : 0,0003%).

## 3. Récupération de l’identifiant unique et de tous les pseudo-NIR associés à partir de la liste des pseudo-NIR de l’extraction princeps 

Création de la table REF_NIR_CLE incluant tous les identifiants uniques et les pseudo-NIR associés à partir de la liste des pseudo-NIR de l’extraction princeps.

## 4. Vérification des données démographiques âge et sexe et suppression des identifiants avec données manquantes

Création de la table REF_NIR_DEM après exclusion des données d’année/mois de naissance et de sexe aberrantes ou changeantes pour une même identifiant unique.

## 5. Identification et nettoyage des données de décès

Création de la table REF_NIR_DC incluant les données de décès des patients. Si plusieurs dates existent au sein d’une même source (référentiel patient ou CépiDC) la plus anciennes est sélectionnées. Si plusieurs dates existent en croisant plusieurs sources, la priorisation sur la date CépiDC est effectuée. Les dates de décès aberrantes sont exclues (année-mois de décès < année-mois de naissance, année-mois de décès < année-mois du jour).

## 6. Créations d’indicateurs annuels d’affiliation aux différents régimes

Création de la table REF_NIR_AFF incluant pour chaque année demandées l’affiliation au régime d’Assurance Maladie (au moins une fois dans l’année, avec priorisation suivante en cas de plusieurs régimes RG > SLM > RSI > MSA > Autres > Non Affilié) et l’affiliation à la CMU/AME (au moins une fois dans l’année). Les informations d’affiliations proviennent des tables annuelles CONSOPAT. Extractions_PatientsTR ; en cas d’absence de la table, celle-ci est recréée à partir des informations de remboursements.

Création de la table REF_NIR_TOT incluant toutes les informations recensées depuis la première étape.
