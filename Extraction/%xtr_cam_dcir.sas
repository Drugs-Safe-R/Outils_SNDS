/************************************************************************************************
*	Macro  : xtr_cam_dcir																		*
*	*********************************************************************************************
*	Apache License : Copyright 2023, AHeaD Team Bordeaux Population Health U1219				*
*	*********************************************************************************************
*	Mises  jour : (le cas echeant : preciser date, auteur, et objet de la MAJ)					*
*		23 nov. 2018 - MA : MAJ suite modification %multi_equal_str									*
*		25 jan. 2019 - MA : suppression des filtres sur le mode de sortie dans les extractions  *
*							sur les donnees PMSI-MCO et PMSI-HAD								*
*		23 jul. 2020 - AG : Correction des filtres CCAM avec mise en place						*
*							stp=PROC pour les MCO												*
*		03 aou. 2020 - EH : Correction fusion table presta et opt_pop							*		
*		03 nov. 2022 - PYD : Ajout des autres sources de donnees CCAM 							*
*									MCO : FM, FMSTC												*
*									HAD : FM													*
*									SSR : CCAM, FM, FMSTC										*
*									PSY : CCAM, FM												*
* 		07 nov. 2022 - PYD : Definition automatique annee archivage donnees prestation			*
* 		03 aou. 2023 - CM : Covumentation amelioree + Simplification du programme				*
*		14 mars 2024 - JP : Correction du changement de nom en 2019 en MCO pour DEL_DAT_ENT 	*
*						+ la variable n'existait pas dans FMSTC avant 2012 en SSR et MCO 		*
*						+ variable ENT_DAL_DEL en MCOA a chang� de nature (alphanum � num) 		*
*						� partir de 2011  														*
*	*********************************************************************************************
*	Description : Cette macro permet d'extraire, dans les donnees du SNDS et durant une 		*
*                 periode de temps donne, les actes medicaux realises identifies  partir des	*  
*                 codes CCAM.																	*
*				  Si aucun code n'est renseigne, la macro extrait tous les actes medicaux 		*
*                 realises sur la periode donnee. 												*
*	*********************************************************************************************
*	Remarques : Cette macro extrait les donn�es CCAM depuis plusieurs sources du PSMI (Tables A *
*				FMSTC, CCAM), mais aussi du DCIR (PRS). Des doublons peuvent apparaitrent dans  *
*				la sortie. Un tri selon vos pr�f�rences est donc � r�aliser par la suite.		*
*																								*
*				Une erreur persiste � cause de la variable ENT_DAL_DEL dans MCOA uniquement 	*
*				en 2007, mais n'emp�che en rien l'utilisation de la macro et l'exploitation 	*				
*				des donn�es pour la date de l'acte et le code CCAM, le d�lai n'�tant pas une 	*
*				donn�e fiable au cours du temps, il est recommand� de le recalculer � partir 	*
*				des dates d'entr�e de s�jour ou d'UM et la date de l'acte						*
*	*********************************************************************************************
*	Paramtres :																					*
*		lib_prj 	: nom de la bibliothque ou sera range la table de sortie					*
*		tab_out 	: nom de la table de sortie													*
*		ann_deb 	: annee de debut de la periode d'extraction (format YYYY)					*
*		ann_fin 	: annee de fin de la periode d'extraction (format YYYY)						*
*		opt_pop 	: optionnel : table de population  "jointer" (ex. SASDATA1.POP)				*
*		opt_cam_inc : optionnel : liste des codes CCAM des actes medicaux  extraire				*
*		opt_cam_exc : optionnel : liste des codes CCAM des actes medicaux  ne pas extraire		*
*	*********************************************************************************************
*	R�sultat : table contenant les variables suivantes :										*
*		BEN_NIR_PSA : numro d'identifiant de la personne                          				*
*		EXE_SOI_DTD : date de realisation de l'acte medical du medicament                   	*
*																								*
*	/!\	DEL_DAT_ENT : d�lai entre l'entr�e et l'acte											*
*					/!\ de 2009 � 2018 inclus il s'agit du d�lai /!\							*
*							entre l'entr�e dans l'unit� m�dicale et l'acte						*
*					/!\ � partir de 2019, il s'agit du d�lai /!\								*
*							entre l'entr�e dans le s�jour hospitalier et l'acte					*
*				Il est conseill� de recalculer ce d�lai si vous en avez besoin 					*
*																								*
*		CAM_COD	    : 	code CCAM (7 caractres) de l'acte mdical realise						*
*		CAM_LIB	    : 	libelle CCAM de l'acte medical realise									*
*		ETA_NUM		: 	N�FINESS																*
*		ETA_NUM_EPMSI:	N� FINESS e-PMSI														*
*		RHAD_NUM	:	N� s�quentiel de s�jour HAD												*
*		RHA_NUM 	:	N� S�quentiel du s�jour SSR												*
*		RSA_NUM		: 	N� d'index du RSA														*
*		RIP_NUM		: 	N� S�quentiel du s�jour PSY												*
*		SEQ_NUM		:	N� de la s�quence dans le s�jour										*
*		CAM_OGN		:	origine des donnes extraites (PRS, SSR, MCO, ou HAD)					*
*		TAB_OGN		: 	table dans le snds � l'origine ddes donnes extraites					*
*	*********************************************************************************************
*	Exemple :																					*
*	%xtr_cam_dcir(lib_prj = WORK, tab_out = EXTRACT_CCAM, ann_deb = 2009, ann_fin = 2013,		*
*			opt_pop =, opt_cam_inc = GFBA, opt_cam_exc = GFBA001);								*
*		Cet exemple extrait	les actes CCAM entre 2009 et 2013 cod�s GFBA hormis GFBA001 sur		*
*		sur toute la population du DCIR. Les donn�es sont stock�es dans la Work dans la table	*
*		d�nomm�e EXTRACT_CCAM																	*
************************************************************************************************/

options nomprint nosymbolgen;
option fullstimer debug=dbms_timers sastrace=',,,dq' sastraceloc=saslog nostsuffix msglevel=I;

%macro xtr_cam_dcir(lib_prj, tab_out, ann_deb, ann_fin, opt_pop, opt_cam_inc, opt_cam_exc);

	/* Macro de suppression automatique des tables mentionnees sur ORAUSER */
	%macro checkDrop(myData);
		%if %SYSFUNC(exist(orauser.&myData)) %then %do;
		    proc sql;
				%connectora;
				EXECUTE(
			   		drop table &myData
				)
			BY ORACLE;
		    quit;
		%end;
	%mend;	

	/*Definition auto annee archivage donnees prestation*/
	%let ann_act = %sysfunc(year(%sysfunc(today())));
	%let aa = 2006;
	%let exist = 1;
	%do %while (&exist. = 1);
		%if %sysfunc(exist(oravue.er_prs_f_&aa.))=1 %then %do;
			%let exist = 1;
			%let aa = %eval(&aa.+1);
		%end;
		%else %do;
			%let exist = 0;
		%end;
	%end;
	%let ann_arc = %eval(&aa.-1);

	/* Macro permettant la jointure des 9 tables de cles primaires */
	%macro join(t1,t2);
			&t1..DCT_ORD_NUM=&t2..DCT_ORD_NUM AND &t1..FLX_DIS_DTD=&t2..FLX_DIS_DTD AND &t1..FLX_EMT_ORD=&t2..FLX_EMT_ORD AND
			&t1..FLX_EMT_NUM=&t2..FLX_EMT_NUM AND &t1..FLX_EMT_TYP=&t2..FLX_EMT_TYP AND &t1..FLX_TRT_DTD=&t2..FLX_TRT_DTD AND
			&t1..ORG_CLE_NUM=&t2..ORG_CLE_NUM AND &t1..PRS_ORD_NUM=&t2..PRS_ORD_NUM AND &t1..REM_TYP_AFF=&t2..REM_TYP_AFF
	%mend join;

	/* Macro permettant la selection, notamment sur les filtres, de la liste de codes */	
	%macro multi_equal_str(var, lst_cod, aim, stp);

		%local i val ope;
		%let i = 1;
		%let val =;
		%let ope =;

		%do %until (%superq(val) =);

			%let val = %qscan(&lst_cod., &i., %str( ));

			%if %sysfunc(verify(&val., '0123456789')) > 0 %then %do;
				%if &aim. = INC %then %do;
					%if &stp. = PROC %then %do;
						%if %superq(val) > 0 %then %do;
						  %cmpres(%str(&ope. %upcase(&var.) like %'%upcase(&val.)%%%'))
						%end;
						%put %str(&ope. %upcase(&var.) like %'%upcase(%superq(val))%%%');
					%end;
					%if &stp. = DATA %then %do;
						%if %superq(val) > 0 %then %do;
						  %cmpres(%str(&ope. %upcase(&var.) =: %'%upcase(&val.)%'))
						%end;
						%put %str(&ope. %upcase(&var.) =: %'%upcase(%superq(val))%');
					%end;
					%let ope = or;
				%end;

				%if &aim. = EXC %then %do;
					%if &stp. = PROC %then %do;
						%if %superq(val) > 0 %then %do;
						  %cmpres(%str(&ope. %upcase(&var.) not like %'%upcase(&val.)%%%'))
						%end;
						%put %str(&ope. %upcase(&var.) not like %'%upcase(%superq(val))%%%');
					%end;
					%if &stp. = DATA %then %do;
						%if %superq(val) > 0 %then %do;
						  %cmpres(%str(&ope. %upcase(&var.) ^=: %'%upcase(&val.)%'))
						%end;
						%put %str(&ope. %upcase(&var.) ^=: %'%upcase(%superq(val))%');
					%end;
					%let ope = and;
				%end;
			%end;

			%if %sysfunc(verify(&val., '0123456789')) = 0 %then %do;
				%if &aim. = INC %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. (&var.) = (&val.)))
					%end;
					%put %str(&ope. (&var.) = (%superq(val)));
					%let ope = or;
				%end;
				%if &aim. = EXC %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. (&var.) ^= (&val.)))
					%end;
					%put %str(&ope. (&var.) ^= (%superq(val)));
					%let ope = and;
				%end;
			%end;

			%let i = %eval(&i. + 1);
		%end;

	%mend;


/* Selection de la population si renseignee dans l'appel de la macro */
	%if &opt_pop.>0 %then %do;
			/*Copie table pop vers orauser*/
		%if %sysfunc(exist(orauser.&opt_pop.))=0 %then %do;
			proc sql;
				create table orauser.&opt_pop. (BULKLOAD=yes BL_DATAFILE="%sysfunc(pathname(work))/data.dat" BL_DELETE_DATAFILE=yes) 
					as select * from &lib_prj..&opt_pop.;
			quit;
		%end;
		%else %do;
			%checkDrop(&opt_pop.);
			proc sql;
				create table orauser.&opt_pop. (BULKLOAD=yes BL_DATAFILE="%sysfunc(pathname(work))/data.dat" BL_DELETE_DATAFILE=yes) 
					as select * from &lib_prj..&opt_pop.;
			quit;
		%end;
	%end;


/* ----------------------- */
/* -        MACRO        - */
/* ----------------------- */

	%if (&opt_cam_inc. > 0 or &opt_cam_exc. > 0) or (&opt_cam_inc. = and &opt_cam_exc. = and &opt_pop. > 0) %then %do;

		/* Initialisation de la table des resultats */
		%checkDrop(TPR_CAM);
		proc sql;
			create table orauser.TPR_CAM 
					(BEN_NIR_PSA char(17), EXE_SOI_DTD date, DEL_DAT_ENT num(5),CAM_COD char(10), ETA_NUM char(9), ETA_NUM_EPMSI char(9), RHAD_NUM char(5),
					 RHA_NUM char(7), RSA_NUM char(10), RIP_NUM char(6), SEQ_NUM char(8), CAM_OGN char(3), TAB_OGN char(5));
		quit;

		/* Annee actuelle du requetage */
		%let ann_act = %sysfunc(year(%sysfunc(today())));


	/* Donnees des actes medicaux realises en milieu hospitalier, par annee */
		%do aaaa = %sysfunc(max(&ann_deb., 2006)) %to %sysfunc(min(&ann_fin., &ann_act.));
			%let aa = %substr(&aaaa., 3, 2);

			/* Initialisation de la jointure avec la table population */
			%if &opt_pop. > 0 %then %do;
				%let jnt_mco_pop = '&opt_pop. t_pop inner join T_MCO&aa.C t_mcoC on t_pop.BEN_NIR_PSA = t_mcoC.NIR_ANO_17';
				%let jnt_mcostc_pop = '&opt_pop. t_pop inner join T_MCO&aa.CSTC t_mcoCSTC on t_pop.BEN_NIR_PSA = t_mcoCSTC.NIR_ANO_17';
				%let jnt_had_pop = '&opt_pop. t_pop inner join T_HAD&aa.C t_hadC on t_pop.BEN_NIR_PSA = t_hadC.NIR_ANO_17';
			%end;
			%else %do;
				%let jnt_mco_pop = 'T_MCO&aa.C t_mcoC';
				%let jnt_mcostc_pop = 'T_MCO&aa.CSTC t_mcoCSTC';
				%let jnt_had_pop = 'T_HAD&aa.C t_hadC';
			%end;

			/* Initialisation des filtres sur les codes CCAM � inclure et/ou exclure */
			%if &opt_cam_inc. > 0 %then %do;
				%if &opt_cam_exc. > 0 %then %do;
					%let flt_mco_cam = '(%multi_equal_str(var = t_mcoA.CDC_ACT, lst_cod = &opt_cam_inc., aim = INC, stp = PROC)) and (%multi_equal_str(var = t_mcoA.CDC_ACT, lst_cod = &opt_cam_exc., aim = EXC, stp = PROC))';
					%let flt_mco_fm_cam = '(%multi_equal_str(var = CCAM_COD, lst_cod = &opt_cam_inc., aim = INC, stp = PROC)) and (%multi_equal_str(var = CCAM_COD, lst_cod = &opt_cam_exc., aim = EXC, stp = PROC))';
					%let flt_had_cam = '(%multi_equal_str(var = CCAM_COD, lst_cod = &opt_cam_inc., aim = INC, stp = PROC)) and (%multi_equal_str(var = CCAM_COD, lst_cod = &opt_cam_exc., aim = EXC, stp = PROC))';
					%let flt_rip_cam = '(%multi_equal_str(var = CCAM_COD, lst_cod = &opt_cam_inc., aim = INC, stp = PROC)) and (%multi_equal_str(var = CCAM_COD, lst_cod = &opt_cam_exc., aim = EXC, stp = PROC))';
					%let flt_rip_ccam_cam = '(%multi_equal_str(var = CDC_ACT, lst_cod = &opt_cam_inc., aim = INC, stp = PROC)) and (%multi_equal_str(var = CDC_ACT, lst_cod = &opt_cam_exc., aim = EXC, stp = PROC))';
					%let flt_ssr_cam = '(%multi_equal_str(var = CCAM_COD, lst_cod = &opt_cam_inc., aim = INC, stp = PROC)) and (%multi_equal_str(var = CCAM_COD, lst_cod = &opt_cam_exc., aim = EXC, stp = PROC))';
					%let flt_ssr_ccam_cam = '(%multi_equal_str(var = CCAM_ACT, lst_cod = &opt_cam_inc., aim = INC, stp = PROC)) and (%multi_equal_str(var = CCAM_ACT, lst_cod = &opt_cam_exc., aim = EXC, stp = PROC))';
				%end;		
				%else %do;
					%let flt_mco_cam = '(%multi_equal_str(var = t_mcoA.CDC_ACT, lst_cod = &opt_cam_inc., aim = INC, stp = PROC))';
					%let flt_mco_fm_cam = '(%multi_equal_str(var = CCAM_COD, lst_cod = &opt_cam_inc., aim = INC, stp = PROC))';
					%let flt_had_cam = '(%multi_equal_str(var = CCAM_COD, lst_cod = &opt_cam_inc., aim = INC, stp = PROC))';
					%let flt_rip_cam = '(%multi_equal_str(var = CCAM_COD, lst_cod = &opt_cam_inc., aim = INC, stp = PROC))';
					%let flt_rip_ccam_cam = '(%multi_equal_str(var = CDC_ACT, lst_cod = &opt_cam_inc., aim = INC, stp = PROC))';
					%let flt_ssr_cam = '(%multi_equal_str(var = CCAM_COD, lst_cod = &opt_cam_inc., aim = INC, stp = PROC))';
					%let flt_ssr_ccam_cam = '(%multi_equal_str(var = CCAM_ACT, lst_cod = &opt_cam_inc., aim = INC, stp = PROC))';
				%end;
			%end;
			%else %do;
				%if &opt_cam_exc. > 0 %then %do;
					%let flt_mco_cam = '(%multi_equal_str(var = t_mcoA.CDC_ACT, lst_cod = &opt_cam_exc., aim = EXC, stp = PROC))';
					%let flt_mco_fm_cam = '(%multi_equal_str(var = t_mcoFM.CCAM_COD, lst_cod = &opt_cam_exc., aim = EXC, stp = PROC))';
					%let flt_had_cam = '(%multi_equal_str(var = CCAM_COD, lst_cod = &opt_cam_exc., aim = EXC, stp = PROC))';
					%let flt_rip_cam = '(%multi_equal_str(var = CCAM_COD, lst_cod = &opt_cam_exc., aim = EXC, stp = PROC))';
					%let flt_rip_ccam_cam = '(%multi_equal_str(var = CDC_ACT, lst_cod = &opt_cam_exc., aim = EXC, stp = PROC))';
					%let flt_ssr_cam = '(%multi_equal_str(var = CCAM_COD, lst_cod = &opt_cam_exc., aim = EXC, stp = PROC))';
					%let flt_ssr_ccam_cam = '(%multi_equal_str(var = CCAM_ACT, lst_cod = &opt_cam_exc., aim = EXC, stp = PROC))';					
				%end;
				%else %do;
					%let flt_mco_cam = '';
					%let flt_mco_fm_cam = '';
					%let flt_had_cam = '';
					%let flt_rip_cam = '';
					%let flt_rip_ccam_cam = '';
					%let flt_ssr_cam = '';
					%let flt_ssr_ccam_cam = '';
				%end;
			%end;

		/* Requetage sur les donn�es issues des SSR */
			/* Initialisation du parametre var_soi_ssr manquant partiellement si donnees selectionnees avant 2008 */
			%if &aaaa. <= 2008 %then %do;
				%let var_soi_ssr = *to_date('15-' || T_SSRC.SOR_MOI || '-' || T_SSRC.SOR_ANN)*;
			%end;
			%else %do;
				%let var_soi_ssr = *T_SSRC.EXE_SOI_DTD*;
			%end;


			/* Requetage sur les tables CCAM */
			%if %sysfunc(exist(oravue.T_SSR&aa.CCAM))=1  %then %do;
			%if &aaaa. = 2009 %then %do;
					proc sql;
						%connectora;
						EXECUTE(
						%if %sysfunc(exist(orauser.TPR_CAM))=0 %then %do; create table TPR_CAM as %end; %else %do; insert into TPR_CAM %end;
						(select distinct T_SSRC.NIR_ANO_17 as BEN_NIR_PSA,
								%scan(&var_soi_ssr, 1, "*") as EXE_SOI_DTD,
								'' as DEL_DAT_ENT,
								TRIM(T_SSRCCAM.CCAM_ACT) as CAM_COD,
								T_SSRC.ETA_NUM,
								'' as ETA_NUM_EPMSI,
								'' as RHAD_NUM,
								T_SSRC.RHA_NUM,
								'' as RSA_NUM,
								'' as RIP_NUM,
								'' as SEQ_NUM,
								'SSR' as CAM_OGN,
								'CCAM' as TAB_OGN
						from T_SSR&aa.C T_SSRC
							inner join T_SSR&aa.CCAM T_SSRCCAM on (T_SSRC.ETA_NUM = T_SSRCCAM.ETA_NUM and T_SSRC.RHA_NUM = T_SSRCCAM.RHA_NUM)	
						where (%scan(&flt_ssr_ccam_cam., 1, "'"))))
						BY ORACLE;
					quit;
				%end;
				%if &aaaa. > 2009 and &aaaa. <= 2019 %then %do;
					proc sql;
						%connectora;
						EXECUTE(
						%if %sysfunc(exist(orauser.TPR_CAM))=0 %then %do; create table TPR_CAM as %end; %else %do; insert into TPR_CAM %end;
						(select distinct T_SSRC.NIR_ANO_17 as BEN_NIR_PSA,
								%scan(&var_soi_ssr, 1, "*") as EXE_SOI_DTD,
								T_SSRCCAM.CCAM_DEL_ENT_UM as DEL_DAT_ENT,
								TRIM(T_SSRCCAM.CCAM_ACT) as CAM_COD,
								T_SSRC.ETA_NUM,
								'' as ETA_NUM_EPMSI,
								'' as RHAD_NUM,
								T_SSRC.RHA_NUM,
								'' as RSA_NUM,
								'' as RIP_NUM,
								'' as SEQ_NUM,
								'SSR' as CAM_OGN,
								'CCAM' as TAB_OGN
						from T_SSR&aa.C T_SSRC
							inner join T_SSR&aa.CCAM T_SSRCCAM on (T_SSRC.ETA_NUM = T_SSRCCAM.ETA_NUM and T_SSRC.RHA_NUM = T_SSRCCAM.RHA_NUM)	
						where (%scan(&flt_ssr_ccam_cam., 1, "'"))))
						BY ORACLE;
					quit;
				%end;
				%if &aaaa. > 2019 %then %do;
					proc sql;
						%connectora;
						EXECUTE(
						%if %sysfunc(exist(orauser.TPR_CAM))=0 %then %do; create table TPR_CAM as %end; %else %do; insert into TPR_CAM %end;
						(select distinct T_SSRC.NIR_ANO_17 as BEN_NIR_PSA,
								%scan(&var_soi_ssr, 1, "*") as EXE_SOI_DTD,
								T_SSRCCAM.DEL_DAT_ENT ,
								TRIM(T_SSRCCAM.CCAM_ACT) as CAM_COD,
								T_SSRC.ETA_NUM,
								'' as ETA_NUM_EPMSI,
								'' as RHAD_NUM,
								T_SSRC.RHA_NUM,
								'' as RSA_NUM,
								'' as RIP_NUM,
								'' as SEQ_NUM,
								'SSR' as CAM_OGN,
								'CCAM' as TAB_OGN
						from T_SSR&aa.C T_SSRC
							inner join T_SSR&aa.CCAM T_SSRCCAM on (T_SSRC.ETA_NUM = T_SSRCCAM.ETA_NUM and T_SSRC.RHA_NUM = T_SSRCCAM.RHA_NUM)	
						where (%scan(&flt_ssr_ccam_cam., 1, "'"))))
						BY ORACLE;
					quit;
				%end;
			%end;

			/* Requetage sur les tables FMSTC */
			%if %sysfunc(exist(oravue.T_SSR&aa.CSTC))=1 and %sysfunc(exist(oravue.T_SSR&aa.FMSTC))=1 %then %do;
				%if &aaaa. = 2012 %then %do;
					proc sql;
						%connectora;
						EXECUTE(
						%if %sysfunc(exist(orauser.TPR_CAM))=0 %then %do; create table TPR_CAM as %end; %else %do; insert into TPR_CAM %end;
						(select distinct T_SSRCSTC.NIR_ANO_17 as BEN_NIR_PSA,
								T_SSRCSTC.EXE_SOI_DTD,
								'' as DEL_DAT_ENT,
								TRIM(T_SSRFMSTC.CCAM_COD) as CAM_COD,
								T_SSRCSTC.ETA_NUM,
								'' as ETA_NUM_EPMSI,
								'' as RHAD_NUM,
								'' as RHA_NUM,
								'' as RSA_NUM,
								'' as RIP_NUM,
								T_SSRCSTC.SEQ_NUM,
								'SSR' as CAM_OGN,
								'FMSTC' as TAB_OGN
						from T_SSR&aa.CSTC T_SSRCSTC
							inner join T_SSR&aa.FMSTC T_SSRFMSTC on (T_SSRCSTC.ETA_NUM = T_SSRFMSTC.ETA_NUM and T_SSRCSTC.SEQ_NUM = T_SSRFMSTC.SEQ_NUM)	
						where (%scan(&flt_ssr_cam., 1, "'"))))
						BY ORACLE;
					quit;
				%end;
				%if &aaaa. > 2012 %then %do;
					proc sql;
						%connectora;
						EXECUTE(
						%if %sysfunc(exist(orauser.TPR_CAM))=0 %then %do; create table TPR_CAM as %end; %else %do; insert into TPR_CAM %end;
						(select distinct T_SSRCSTC.NIR_ANO_17 as BEN_NIR_PSA,
								T_SSRCSTC.EXE_SOI_DTD,
								T_SSRFMSTC.DEL_DAT_ENT,
								TRIM(T_SSRFMSTC.CCAM_COD) as CAM_COD,
								T_SSRCSTC.ETA_NUM,
								'' as ETA_NUM_EPMSI,
								'' as RHAD_NUM,
								'' as RHA_NUM,
								'' as RSA_NUM,
								'' as RIP_NUM,
								T_SSRCSTC.SEQ_NUM,
								'SSR' as CAM_OGN,
								'FMSTC' as TAB_OGN
						from T_SSR&aa.CSTC T_SSRCSTC
							inner join T_SSR&aa.FMSTC T_SSRFMSTC on (T_SSRCSTC.ETA_NUM = T_SSRFMSTC.ETA_NUM and T_SSRCSTC.SEQ_NUM = T_SSRFMSTC.SEQ_NUM)	
						where (%scan(&flt_ssr_cam., 1, "'"))))
						BY ORACLE;
					quit;
				%end;
			%end;

		/* Requetage sur les donn�es issues des MCO */
			/* Initialisation du parametre var_soi_mco manquant partiellement si donnees selectionnees avant 2008 */
			%if &aaaa. <= 2008 %then %do;
				%let var_soi_mco = *to_date('15-' || t_mcoC.SOR_MOI || '-' || t_mcoC.SOR_ANN)*;
			%end;
			%else %do;
				%let var_soi_mco = *t_mcoC.EXE_SOI_DTD*;
			%end;

			/* Requetage sur les tables A*/
			%if %sysfunc(exist(oravue.T_MCO&aa.A))=1 %then %do;
				/* avant 2011, les variables ENT_DAT_DEL sont en alphanum�rique */
				%if &aaaa. < 2011 %then %do;
					proc sql;
					%connectora;
					EXECUTE(
						%if %sysfunc(exist(orauser.TPR_CAM))=0 %then %do; create table TPR_CAM as %end; %else %do; insert into TPR_CAM %end;
						select distinct t_mcoC.NIR_ANO_17 as BEN_NIR_PSA,
										%scan(&var_soi_mco, 1, "*") as EXE_SOI_DTD,
										to_number(t_mcoA.ENT_DAT_DEL) as ENT_DAT_DEL,
									    TRIM(t_mcoA.CDC_ACT) as CAM_COD,
										t_mcoB.ETA_NUM,
										'' as ETA_NUM_EPMSI,
										'' as RHAD_NUM,
										'' as RHA_NUM,
										t_mcoB.RSA_NUM,
										'' as RIP_NUM,
										'' as SEQ_NUM,
										'MCO' as CAM_OGN,
										'A' as TAB_OGN
						from %scan(&jnt_mco_pop., 1, "'") inner join T_MCO&aa.B t_mcoB on (t_mcoC.ETA_NUM = t_mcoB.ETA_NUM and t_mcoC.RSA_NUM = t_mcoB.RSA_NUM)
												   		  inner join T_MCO&aa.A t_mcoA on (t_mcoC.ETA_NUM = t_mcoA.ETA_NUM and t_mcoC.RSA_NUM = t_mcoA.RSA_NUM)
						where 
							%scan(&flt_mco_cam., 1, "'") and
							(t_mcoC.ETA_NUM not in ('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', '750041543', '750100018',
											    	'750100042', '750100075', '750100109', '750100125', '750100166', '750100208', '750100216', '750100232',
											    	'750100273', '750100299', '750801441', '750803447', '750803454', '910100015', '910100023', '920100013',
											    	'920100021', '920100039', '920100047', '920100054', '920100062', '930100011', '930100037', '930100045',
											    	'940100027', '940100035', '940100043', '940100050', '940100068', '950100016', '690783154', '690784137',
											    	'690784152', '690784178', '690787478', '830100558')
							and not (t_mcoB.GRG_GHM = '90H01Z' and t_mcoB.GRG_RET = '024')
							and t_mcoB.GRG_GHM not in ('90H01Z', '90Z00Z', '90Z01Z', '90Z02Z', '90Z03Z')
							and ((t_mcoB.SEJ_TYP = 'A' or t_mcoB.SEJ_TYP is null) or (t_mcoB.SEJ_TYP = 'B' and t_mcoB.GRG_GHM not in ('28Z14Z', '28Z15Z', '28Z16Z')))))
					BY ORACLE;
					quit;
				%end;
				%if &aaaa. >= 2011 %then %do;
					proc sql;
					%connectora;
					EXECUTE(
						%if %sysfunc(exist(orauser.TPR_CAM))=0 %then %do; create table TPR_CAM as %end; %else %do; insert into TPR_CAM %end;
						select distinct t_mcoC.NIR_ANO_17 as BEN_NIR_PSA,
										%scan(&var_soi_mco, 1, "*") as EXE_SOI_DTD,
										t_mcoA.ENT_DAT_DEL,
									    TRIM(t_mcoA.CDC_ACT) as CAM_COD,
										t_mcoB.ETA_NUM,
										'' as ETA_NUM_EPMSI,
										'' as RHAD_NUM,
										'' as RHA_NUM,
										t_mcoB.RSA_NUM,
										'' as RIP_NUM,
										'' as SEQ_NUM,
										'MCO' as CAM_OGN,
										'A' as TAB_OGN
						from %scan(&jnt_mco_pop., 1, "'") inner join T_MCO&aa.B t_mcoB on (t_mcoC.ETA_NUM = t_mcoB.ETA_NUM and t_mcoC.RSA_NUM = t_mcoB.RSA_NUM)
												   		  inner join T_MCO&aa.A t_mcoA on (t_mcoC.ETA_NUM = t_mcoA.ETA_NUM and t_mcoC.RSA_NUM = t_mcoA.RSA_NUM)
						where 
							%scan(&flt_mco_cam., 1, "'") and
							(t_mcoC.ETA_NUM not in ('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', '750041543', '750100018',
											    	'750100042', '750100075', '750100109', '750100125', '750100166', '750100208', '750100216', '750100232',
											    	'750100273', '750100299', '750801441', '750803447', '750803454', '910100015', '910100023', '920100013',
											    	'920100021', '920100039', '920100047', '920100054', '920100062', '930100011', '930100037', '930100045',
											    	'940100027', '940100035', '940100043', '940100050', '940100068', '950100016', '690783154', '690784137',
											    	'690784152', '690784178', '690787478', '830100558')
							and not (t_mcoB.GRG_GHM = '90H01Z' and t_mcoB.GRG_RET = '024')
							and t_mcoB.GRG_GHM not in ('90H01Z', '90Z00Z', '90Z01Z', '90Z02Z', '90Z03Z')
							and ((t_mcoB.SEJ_TYP = 'A' or t_mcoB.SEJ_TYP is null) or (t_mcoB.SEJ_TYP = 'B' and t_mcoB.GRG_GHM not in ('28Z14Z', '28Z15Z', '28Z16Z')))))
					BY ORACLE;
					quit;
				%end;
			%end; 
			/* Requetage sur les tables FMSTC */
			%if %sysfunc(exist(oravue.T_MCO&aa.FMSTC))=1 and %sysfunc(exist(oravue.T_MCO&aa.CSTC))=1 and &aaaa.>=2009 %then %do;
				%if &aaaa. <= 2012 %then %do;
					proc sql;
					%connectora;
					EXECUTE(
						%if %sysfunc(exist(orauser.TPR_CAM))=0 %then %do; create table TPR_CAM as %end; %else %do; insert into TPR_CAM %end;
						select distinct t_mcoCSTC.NIR_ANO_17 as BEN_NIR_PSA,
										t_mcoCSTC.EXE_SOI_DTD,
										'' as DEL_DAT_ENT,
									    TRIM(t_mcoFMSTC.CCAM_COD) as CAM_COD,
										t_mcoFMSTC.ETA_NUM,
										'' as ETA_NUM_EPMSI,
										'' as RHAD_NUM,
										'' as RHA_NUM,
										'' as RSA_NUM,
										'' as RIP_NUM,
										t_mcoFMSTC.SEQ_NUM,
										'MCO' as CAM_OGN,
										'FMSTC' as TAB_OGN
						from %scan(&jnt_mcostc_pop., 1, "'") inner join T_MCO&aa.FMSTC t_mcoFMSTC on (t_mcoCSTC.ETA_NUM = t_mcoFMSTC.ETA_NUM and t_mcoCSTC.SEQ_NUM = t_mcoFMSTC.SEQ_NUM)
						where 
							%scan(&flt_mco_fm_cam., 1, "'") and
							(t_mcoCSTC.ETA_NUM not in ('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', '750041543', '750100018',
											    	'750100042', '750100075', '750100109', '750100125', '750100166', '750100208', '750100216', '750100232',
											    	'750100273', '750100299', '750801441', '750803447', '750803454', '910100015', '910100023', '920100013',
											    	'920100021', '920100039', '920100047', '920100054', '920100062', '930100011', '930100037', '930100045',
											    	'940100027', '940100035', '940100043', '940100050', '940100068', '950100016', '690783154', '690784137',
											    	'690784152', '690784178', '690787478', '830100558')))
					BY ORACLE;
					quit;
				%end; 
				%if &aaaa. > 2012 %then %do;
					proc sql;
					%connectora;
					EXECUTE(
						%if %sysfunc(exist(orauser.TPR_CAM))=0 %then %do; create table TPR_CAM as %end; %else %do; insert into TPR_CAM %end;
						select distinct t_mcoCSTC.NIR_ANO_17 as BEN_NIR_PSA,
										t_mcoCSTC.EXE_SOI_DTD,
										t_mcoFMSTC.DEL_DAT_ENT,
									    TRIM(t_mcoFMSTC.CCAM_COD) as CAM_COD,
										t_mcoFMSTC.ETA_NUM,
										'' as ETA_NUM_EPMSI,
										'' as RHAD_NUM,
										'' as RHA_NUM,
										'' as RSA_NUM,
										'' as RIP_NUM,
										t_mcoFMSTC.SEQ_NUM,
										'MCO' as CAM_OGN,
										'FMSTC' as TAB_OGN
						from %scan(&jnt_mcostc_pop., 1, "'") inner join T_MCO&aa.FMSTC t_mcoFMSTC on (t_mcoCSTC.ETA_NUM = t_mcoFMSTC.ETA_NUM and t_mcoCSTC.SEQ_NUM = t_mcoFMSTC.SEQ_NUM)
						where 
							%scan(&flt_mco_fm_cam., 1, "'") and
							(t_mcoCSTC.ETA_NUM not in ('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', '750041543', '750100018',
											    	'750100042', '750100075', '750100109', '750100125', '750100166', '750100208', '750100216', '750100232',
											    	'750100273', '750100299', '750801441', '750803447', '750803454', '910100015', '910100023', '920100013',
											    	'920100021', '920100039', '920100047', '920100054', '920100062', '930100011', '930100037', '930100045',
											    	'940100027', '940100035', '940100043', '940100050', '940100068', '950100016', '690783154', '690784137',
											    	'690784152', '690784178', '690787478', '830100558')))
					BY ORACLE;
					quit;
				%end; 
			%end;

		
		/* Requetage sur les donn�es issues des HAD */
			/* Requetage sur les tables A */
			%if %sysfunc(exist(oravue.T_HAD&aa.A))=1 %then %do;
				proc sql;
				%connectora;
				EXECUTE(
					%if %sysfunc(exist(orauser.TPR_CAM))=0 %then %do; create table TPR_CAM as %end; %else %do; insert into TPR_CAM %end;
					select distinct t_hadC.NIR_ANO_17 as BEN_NIR_PSA,
								    t_hadC.EXE_SOI_DTD,
									'' as DEL_DAT_ENT,
								    TRIM(t_hadA.CCAM_COD) as CAM_COD,
									'' as ETA_NUM,
									t_hadA.ETA_NUM_EPMSI,
									t_hadA.RHAD_NUM,
									'' as RHA_NUM,
									'' as RSA_NUM,
									'' as RIP_NUM,
									'' as SEQ_NUM,
									'HAD' as CAM_OGN,
									'A' as TAB_OGN
					from %scan(&jnt_had_pop., 1, "'") inner join T_HAD&aa.B t_hadB on (t_hadC.ETA_NUM_EPMSI = t_hadB.ETA_NUM_EPMSI and t_hadC.RHAD_NUM = t_hadB.RHAD_NUM)
											   		  inner join T_HAD&aa.A t_hadA on (t_hadC.ETA_NUM_EPMSI = t_hadA.ETA_NUM_EPMSI and t_hadC.RHAD_NUM = t_hadA.RHAD_NUM)
					where
						%scan(&flt_had_cam., 1, "'") and
						t_hadC.NIR_RET = '0' and
						t_hadC.NAI_RET = '0' and
						t_hadC.SEX_RET = '0' and
						t_hadC.SEJ_RET = '0' and
						t_hadC.FHO_RET = '0' and
						t_hadC.PMS_RET = '0' and
						t_hadC.DAT_RET = '0')
				BY ORACLE;
				quit;
			%end;

		/* Requetage sur les donn�es issues des donnees PSY (RIP) */
			/* Requetage sur les tables CCAM */
			%if %sysfunc(exist(oravue.T_RIP&aa.CCAM))=1 %then %do;
				proc sql;
					%connectora;
					EXECUTE(
					%if %sysfunc(exist(orauser.TPR_CAM))=0 %then %do; create table TPR_CAM as %end; %else %do; insert into TPR_CAM %end;
					(select distinct T_RIPC.NIR_ANO_17 as BEN_NIR_PSA,
							T_RIPC.EXE_SOI_DTD,
							T_RIPCCAM.ENT_DAT_DEL as DEL_DAT_ENT,
							TRIM(T_RIPCCAM.CDC_ACT) as CAM_COD,
							'' as ETA_NUM,
							T_RIPC.ETA_NUM_EPMSI,
							'' as RHAD_NUM,
							'' as RHA_NUM,
							'' as RSA_NUM,
							T_RIPC.RIP_NUM,
							'' as SEQ_NUM,
							'RIP' as CAM_OGN,
							'CCAM' as TAB_OGN
					from T_RIP&aa.C T_RIPC
						inner join T_RIP&aa.CCAM T_RIPCCAM on (T_RIPC.ETA_NUM_EPMSI = T_RIPCCAM.ETA_NUM_EPMSI and T_RIPC.RIP_NUM = T_RIPCCAM.RIP_NUM)	
					where (%scan(&flt_rip_ccam_cam., 1, "'"))))
					BY ORACLE;
				quit;
			%end;

		%end;

		/* Donnees des actes medicaux realises en ambulatoire */

		%do aaaa = %sysfunc(max(&ann_deb., 2006)) %to &ann_fin.;
			%let aaaa_1 = %sysevalf(&aaaa.+1,integer);
			%let tab_xtr_cam = XTR_&tab_out._CAM_&aaaa.;

			%if &aaaa. <= &ann_arc. %then %do;
				%let T_PRS = ER_PRS_F_&aaaa.;
				%let T_CAM = ER_CAM_F_&aaaa.;
			%end;
			%else %do;
				%let T_PRS = ER_PRS_F;
				%let T_CAM = ER_CAM_F;
			%end;


			%if &opt_pop. > 0 %then %do;
				%let jnt_prs_pop = '&opt_pop. t_pop inner join &T_PRS. t_prs on t_pop.BEN_NIR_PSA = t_prs.BEN_NIR_PSA';
			%end;
			%else %do;
				%let jnt_prs_pop = '&T_PRS. t_prs';
			%end;

			%if &opt_cam_inc. > 0 %then %do;
				%if &opt_cam_exc. > 0 %then %do;
					%let flt_prs_cam = '(%multi_equal_str(var = t_cam.CAM_PRS_IDE, lst_cod = &opt_cam_inc., aim = INC, stp = PROC)) and (%multi_equal_str(var = t_cam.CAM_PRS_IDE, lst_cod = &opt_cam_exc., aim = EXC, stp = PROC)) and';
				%end;		
				%else %do;
					%let flt_prs_cam = '(%multi_equal_str(var = t_cam.CAM_PRS_IDE, lst_cod = &opt_cam_inc., aim = INC, stp = PROC)) and';
				%end;
			%end;
			%else %do;
				%if &opt_cam_exc. > 0 %then %do;
					%let flt_prs_cam = '(%multi_equal_str(var = t_cam.CAM_PRS_IDE, lst_cod = &opt_cam_exc., aim = EXC, stp = PROC)) and';
				%end;
				%else %do;
					%let flt_prs_cam = '';
				%end;
			%end;

			/*Periodes d'execution des soins*/
			%let dbt = '01/01/&aaaa';
			%let fin = '31/12/&aaaa';
			%let date_dbt = %sysfunc(compress(&dbt, '/'));
			%let date_fin = %sysfunc(compress(&fin, '/'));

			%if &aaaa. <= &ann_arc. %then %do;
				%let limit = 11;
			%end; 
			%else %do;
				%let limit = 38;
			%end;
			/*annee de flux N*/
			%do i = 0 %to &limit.;

				%let flxDbt=%sysfunc(intnx(MONTH,%sysfunc(inputn(%sysfunc(mdy(1,1,&aaaa.),ddmmyyn8.),ddmmyy8.)),&i),ddmmyyn8.);
				%let flxFin=%sysfunc(intnx(MONTH,%sysfunc(inputn(%sysfunc(mdy(1,1,&aaaa.),ddmmyyn8.),ddmmyy8.)),%eval(&i.+1)),ddmmyyn8.);

				proc sql;
				%connectora;
				EXECUTE(
					%if %sysfunc(exist(orauser.TPR_CAM))=0 %then %do; create table TPR_CAM as %end; %else %do; insert into TPR_CAM %end;
					select distinct t_prs.BEN_NIR_PSA,
								    t_prs.EXE_SOI_DTD as EXE_SOI_DTD,
									'' as DEL_DAT_ENT,
									TRIM(t_cam.CAM_PRS_IDE) as CAM_COD,
									'' as ETA_NUM,
									'' as ETA_NUM_EPMSI,
									'' as RHAD_NUM,
									'' as RHA_NUM,
									'' as RSA_NUM,
									'' as RIP_NUM,
									'' as SEQ_NUM,
									'PRS' as CAM_OGN,
									'' as TAB_OGN
					from %scan(&jnt_prs_pop., 1, "'") inner join &T_CAM. t_cam on %join(t_prs,t_cam)
					where
						%scan(&flt_prs_cam., 1, "'")
						t_prs.EXE_SOI_DTD between to_date(%str(%'&date_dbt.%'), 'ddmmyyyy') and to_date(%str(%'&date_fin.%'), 'ddmmyyyy') and
						t_prs.FLX_DIS_DTD between to_date(%str(%'&flxDbt.%'), 'ddmmyyyy') and to_date(%str(%'&flxFin.%'), 'ddmmyyyy') and
						t_prs.DPN_QLF <> 71)
				BY ORACLE;	
				quit;
			%end;
			/*fin annee de flux N*/	

		%end;		

		/* Recupration des libelles des codes CCAM + Table de sortie */

		/*Periodes d'execution des soins*/
		%let dbt = '01/01/&ann_deb';
		%let date_dbt = %sysfunc(compress(&dbt., '/'));
		%let fin = '31/12/&ann_fin';
		%let date_fin = %sysfunc(compress(&fin., '/'));

		proc sql;
		%connectora;
		EXECUTE(
			create table &tab_out. as
			select distinct t_cam.BEN_NIR_PSA,
						    t_cam.EXE_SOI_DTD,
							t_cam.DEL_DAT_ENT,
						    t_cam.CAM_COD,
							t_ref.ACT_LIB_LNG as CAM_LIB,
							t_cam.ETA_NUM,
							t_cam.ETA_NUM_EPMSI,
							t_cam.RHAD_NUM,
							t_cam.RHA_NUM,
							t_cam.RSA_NUM,
							t_cam.RIP_NUM,
							t_cam.SEQ_NUM,
							t_cam.CAM_OGN,
							t_cam.TAB_OGN
			from TPR_CAM t_cam left join XC_ACT_R t_ref on t_cam.CAM_COD = TRIM(t_ref.ACT_COD)
			where (t_cam.EXE_SOI_DTD between to_date(%str(%'&date_dbt.%'),'ddmmyyyy') and to_date(%str(%'&date_fin.%'),'ddmmyyyy') - 1))
		BY ORACLE;
		quit;

		proc sql;
			create table &lib_prj..&tab_out. as
			select distinct BEN_NIR_PSA,
							datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10.,
							DEL_DAT_ENT,
							CAM_COD,
							CAM_LIB,
							ETA_NUM,
							ETA_NUM_EPMSI,
							RHAD_NUM,
							RHA_NUM, 
							RSA_NUM,
							RIP_NUM,
							SEQ_NUM,
							CAM_OGN,
							TAB_OGN
			from orauser.&tab_out.;
		quit;


		%checkDrop(TPR_CAM);
		%checkDrop(&tab_out.);
		%checkDrop(&opt_pop.);


	%end;

%mend;
