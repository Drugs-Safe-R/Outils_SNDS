

/************************************************************************************************
*	Macro  : multi_like																			*
*	Auteur : Mickael Arnaud																		*
*	Date   : 10 noct. 2018																		*
*	*********************************************************************************************
*	Apache License : Copyright 2023, AHeaD Team Bordeaux Population Health U1219				*
*	*********************************************************************************************
*	Mises a jour : (le cas echeant : preciser date, auteur, et objet de la MAJ)					*
*		22 nov. 2018 - MA : ajout du parametre stp ("STEP"; voir ci-dessous)					*
*	*********************************************************************************************
*	Description : Cette macro permet d'enchainer les operateurs "like" a partir de codes de 	*
*                 longueur variable	present dans une liste de codes a selectionner ou a exclure *
*				  Si les codes sont des nombres, l'operateur "like" est remplace par "="		*
*	*********************************************************************************************
*	Parametres :																				*
*		var 	: nom de la variable ou chercher les codes										*
*		lst_cod : liste de codes a selectionner													*
*		aim 	: les codes doivent-ils etre inclus ou exclus (valeur : INC ou EXC) ?			*
*		stp		: la macro est-elle dans une PROC step ou un DATA step (valeur PROC ou DATA) ?	*
*	*********************************************************************************************
*	Resultat : chaine de caracteres de type "var like 'code_1%' or var like 'code_2%'"			*
*	*********************************************************************************************
*	Exemple :																					*
*		%multi_like(																			*
*			var = t_ref.PHA_ATC_CLA,															*
*			lst_cod = A10BH A10BD07 A10BD08,													*
*			aim = INC, 																			*
*			stp = PROC																			*
*		); 																						*
************************************************************************************************/

/*recherche correspondance pour les variables sous format char*/
%macro multi_like(var, lst_cod, aim, stp);

	%local i val ope;
	%let i = 1;
	%let val =;
	%let ope =;

	%do %until (%superq(val) =);

		%let val = %qscan(&lst_cod., &i., %str( ));

		%if %sysfunc(verify(&val., '0123456789')) > 0 %then %do;
			%if &aim. = INC %then %do;
				%if &stp. = PROC %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. %upcase(&var.) like %'%upcase(&val.)%%%'))
					%end;
					%put %str(&ope. %upcase(&var.) like %'%upcase(%superq(val))%%%');
				%end;
				%if &stp. = DATA %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. %upcase(&var.) =: %'%upcase(&val.)%'))
					%end;
					%put %str(&ope. %upcase(&var.) =: %'%upcase(%superq(val))%');
				%end;
				%let ope = or;
			%end;

			%if &aim. = EXC %then %do;
				%if &stp. = PROC %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. %upcase(&var.) not like %'%upcase(&val.)%%%'))
					%end;
					%put %str(&ope. %upcase(&var.) not like %'%upcase(%superq(val))%%%');
				%end;
				%if &stp. = DATA %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. %upcase(&var.) ^=: %'%upcase(&val.)%'))
					%end;
					%put %str(&ope. %upcase(&var.) ^=: %'%upcase(%superq(val))%');
				%end;
				%let ope = and;
			%end;
		%end;

		%if %sysfunc(verify(&val., '0123456789')) = 0 %then %do;
			%if &aim. = INC %then %do;
				%if %superq(val) > 0 %then %do;
				  %cmpres(%str(&ope. (&var.) = (&val.)))
				%end;
				%put %str(&ope. (&var.) = (%superq(val)));
				%let ope = or;
			%end;
			%if &aim. = EXC %then %do;
				%if %superq(val) > 0 %then %do;
				  %cmpres(%str(&ope. (&var.) ^= (&val.)))
				%end;
				%put %str(&ope. (&var.) ^= (%superq(val)));
				%let ope = and;
			%end;
		%end;

		%let i = %eval(&i. + 1);
	%end;

%mend;
