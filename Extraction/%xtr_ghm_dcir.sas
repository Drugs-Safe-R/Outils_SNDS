/****************************************************************************************************************
*	Macro  : xtr_ghm_dcir																						*
*	Date   : 10 oct 2023																						*
*	*************************************************************************************************************
*	Apache License : Copyright 2023, AHeaD Team Bordeaux Population Health U1219								*
*	*************************************************************************************************************
*	Mises a jour :																								*
*																												*
*	*************************************************************************************************************
*	Description : Cette macro permet d'extraire, dans les donnees du PMSI par annee donnee, les					*
*				GHM identifiees a partir des codes GHM.															*
*				Si aucun code GHM n'est renseigne, la macro extrait tous les GHM realises						*
*				sur la periode donnee, par annee, uniquement si une population est renseignee.					*
*	*************************************************************************************************************
*	Parametres :																								*
*		lib_out 	: nom de la bibliotheque ou sera rangee la table de sortie									*
*		tab_out 	: nom de la table de sortie																	*
*		ann_deb 	: annee de debut de la periode d'extraction (format YYYY)									*
*		ann_fin 	: annee de fin de la periode d'extraction (format YYYY)										*
*																												*
*		opt_pop 	: optionnel : table de population a "jointer" (ex. SASDATA1.POP)							*
*		opt_ghm_inc : optionnel : liste des codes GHM a extraire												*
*		opt_ghm_exc : optionnel : liste des codes GHM a ne pas extraire											*
*																												*
*		opt_suppr 	: optionnel : Supprime les tables annuelles des GHM (1-oui)									*
*		opt_conc 	: optionnel : Concatene les tables annuelles des GHM (1-oui)								*
*	*************************************************************************************************************
*	Remarques																									*
*		La liste des codes doit contenir des espaces entre chaque valeur, exemple : opt_ghm_inc=09C171 09C173	*
*	*************************************************************************************************************
*	Resultat : table contenant les variables suivantes :														*
*		NIR_ANO_17       : numero d'identifiant de la personne                          						*
*		ETA_NUM     	 : numero de l'etablissement (numero FINESS)                      						*
*		RSA_NUM      	 : numero de l'index du RSA				                           						*
*		EXE_SOI_DTD      : date de debut de GHM			                           								*
*		EXE_SOI_DTF      : date de fin de GHM                      				      							*
*		SEJ_NBJ 	     : duree en jours																		*
*		GRG_GHM    		 : Valeur du GHM									                         			*
*	*************************************************************************************************************
*	Exemple :																									*
*	%xtr_ghm_dcir(lib_out=work,tab_out=ghm_13_16,ann_deb=2013,ann_fin=2016,mod_hsp=MH,opt_ghm_inc=09C17,		*
*                opt_ghm_exc=09C174,opt_pop=,opt_suppr=1,opt_conc=1)											*
*																												*
*   Dans cet exemple, seront extraites dans une table unique les GHM MCO de 2013 a 2016 avec les codes GHM		*
*	identifies par 09C17 a l'exclusion de 09C174 C9, avec une suppresion des tables intermediaires pour 		*
*	concatener le resultat dans une seule et meme table ghm_13_16												*
****************************************************************************************************************/

/*Macro multi-like - macro permettant de realiser un equivalent de la fonction %like% */
%macro multi_equal_str(var, lst_cod, aim, stp);

	%local i val ope;
	%let i = 1;
	%let val =;
	%let ope =;

	%do %until (%superq(val) =);

		%let val = %qscan(&lst_cod., &i., %str( ));

		%if %sysfunc(verify(&val., '0123456789')) > 0 %then %do;
			%if &aim. = INC %then %do;
				%if &stp. = PROC %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. %upcase(&var.) like %'%upcase(&val.)%%%'))
					%end;
					%put %str(&ope. %upcase(&var.) like %'%upcase(%superq(val))%%%');
				%end;
				%if &stp. = DATA %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. %upcase(&var.) =: %'%upcase(&val.)%'))
					%end;
					%put %str(&ope. %upcase(&var.) =: %'%upcase(%superq(val))%');
				%end;
				%let ope = or;
			%end;

			%if &aim. = EXC %then %do;
				%if &stp. = PROC %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. %upcase(&var.) not like %'%upcase(&val.)%%%'))
					%end;
					%put %str(&ope. %upcase(&var.) not like %'%upcase(%superq(val))%%%');
				%end;
				%if &stp. = DATA %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. %upcase(&var.) ^=: %'%upcase(&val.)%'))
					%end;
					%put %str(&ope. %upcase(&var.) ^=: %'%upcase(%superq(val))%');
				%end;
				%let ope = and;
			%end;
		%end;

		%if %sysfunc(verify(&val., '0123456789')) = 0 %then %do;
			%if &aim. = INC %then %do;
				%if %superq(val) > 0 %then %do;
				  %cmpres(%str(&ope. (&var.) = (&val.)))
				%end;
				%put %str(&ope. (&var.) = (%superq(val)));	
				%let ope = or;
			%end;
			%if &aim. = EXC %then %do;
				%if %superq(val) > 0 %then %do;
				  %cmpres(%str(&ope. (&var.) ^= (&val.)))
				%end;
				%put %str(&ope. (&var.) ^= (%superq(val)));
				%let ope = and;
			%end;
		%end;	

		%let i = %eval(&i. + 1);
	%end;

%mend;




%macro xtr_ghm_dcir(lib_out=,tab_out=,ann_deb=,ann_fin=,opt_ghm_inc=,opt_ghm_exc=,opt_pop=,opt_suppr=,opt_conc=);

	option fullstimer debug=dbms_timers sastrace=',,,dq' sastraceloc=saslog nostsuffix msglevel=I;
/*Macros suppression tables ORACLE*/
	%macro checkDrop(myTable);
		%if %SYSFUNC(exist(orauser.&myTable)) %then %do;
		    proc sql;
			%connectora;
			EXECUTE(
		   		drop table &myTable
			)
			BY ORACLE;
		    quit;
		%end;
	%mend;

	%macro copie_POP(lib_prj);
		/*copie de la table population de la librairie du projet vers orauser*/
		%if &opt_pop. > 0 %then %do;
			%if %sysfunc(exist(orauser.&opt_pop.))=0 %then %do; 
				proc sql;
					create table orauser.&opt_pop. (BULKLOAD=yes BL_DATAFILE="%sysfunc(pathname(work))/data.dat" BL_DELETE_DATAFILE=yes) 
				    as select * from &lib_prj..&opt_pop.;
				quit;
			%end;
			%else %do;
				%checkDrop(&opt_pop.);
				proc sql;
					create table orauser.&opt_pop. (BULKLOAD=yes BL_DATAFILE="%sysfunc(pathname(work))/data.dat" BL_DELETE_DATAFILE=yes) 
				    as select * from &lib_prj..&opt_pop.;
				quit;
			%end;
		%end;
	%mend;

	%copie_POP(lib_prj=&lib_out.);

/* Messages d'erreur */
	%if &ann_deb. = or &ann_fin. = %then %do;
		%put ERROR: Indiquez un intervalle de temps en annee de minimum 1 an, en completant a la fois la variable ann_deb (i.e. Annee de debut de l extraction) et ann_fin (i.e. Annee de fin de l extraction, incluse).;
		%goto Stop_macro;
	%end;
	%if &lib_out. = or &tab_out. = %then %do;
		%put ERROR: Les parametres lib_out et tab_out sont obligatoires afin de recuperer les donnees dans une table. Veuillez les completer !;
		%goto Stop_macro;
	%end;
	%if &opt_suppr. = 1 and &opt_conc. NE 1 %then %do;
		%put ERROR: Attention, aucune donnee ne peut etre enregistree puisque vous supprimez les tables intermediaires sans les concatener. Pensez a passer le parametre opt_conc a 1 si vous voulez supprimer les tables intermediaires.;
		%goto Stop_macro;
	%end;
	%if &opt_ghm_inc. = and &opt_ghm_exc. = and &opt_pop. = %then %do;
		%put ERROR: Sans au moins un des parametres completes, vous allez extraire toute la table des hospitalisations. Nous preferons ne pas permettre cette operation : une simple Proc SQL permet de dupliquer la table reference;
		%goto Stop_macro;
	%end;	/* Fin des messages d'erreur */


/* Choix de la population */
	%if &opt_pop. > 0 %then %do;
		%let jnt_mco_pop = '&opt_pop. T_POP inner join T_MCO&aa.C T_MCOC on T_POP.BEN_NIR_PSA = T_MCOC.NIR_ANO_17';
	%end;
	%else %do;
		%let jnt_mco_pop = 'T_MCO&aa.C T_MCOC';
	%end;
/* Fin du choix de la population */	


/* Boucle par annee */
%let ann_act = %sysfunc(year(%sysfunc(today())));
%do aaaa = %sysfunc(max(&ann_deb., 2005)) %to %sysfunc(min(&ann_fin., &ann_act. - 1));
	%let aa = %substr(&aaaa., 3, 2);


	/* Choix de la liste des GHM selon les modalites completes dans l'appel de la macro */
		%if %length(&opt_ghm_inc.) > 0 %then %do;
			%if %length(&opt_ghm_exc.) > 0 %then %do;	
				%let flt_ghm_m = '%multi_equal_str(var=T_MCOB.GRG_GHM, lst_cod=&opt_ghm_inc.,aim=INC, stp=PROC) and %multi_equal_str(var=T_MCOB.GRG_GHM, lst_cod=&opt_ghm_exc.,aim=EXC, stp=PROC)';	
			%end;
			%else %do;
				%let flt_ghm_m = '%multi_equal_str(var=T_MCOB.GRG_GHM, lst_cod=&opt_ghm_inc.,aim=INC, stp=PROC)';	
			%end;
		%end;
		%else %do;
			%if %length(&opt_ghm_exc.) > 0 %then %do;
				%let flt_ghm_m = '%multi_equal_str(var=T_MCOB.GRG_GHM, lst_cod=&opt_ghm_exc.,aim=EXC, stp=PROC)';	
			%end;
			%else %do;
				%let flt_ghm_m = 'T_MCOB.GRG_GHM is not NULL';
			%end;
		%end;
	/* Fin du choix de la liste des GHM */


/* ------------- */
/* --- Macro --- */
/* ------------- */
	%if (&opt_ghm_inc. > 0 or &opt_ghm_exc. > 0) or (&opt_ghm_inc. = and &opt_ghm_exc. = and &opt_pop. > 0) %then %do;

			proc sql;
			%connectora;
			EXECUTE(
			create table &tab_out._&aaaa._ghm as
			select distinct T_MCOC.NIR_ANO_17,
							T_MCOB.ETA_NUM,
							T_MCOB.RSA_NUM,
							T_MCOC.EXE_SOI_DTD,
							T_MCOC.EXE_SOI_DTF,
							'MCO' as HSP_OGN,
							CASE WHEN T_MCOB.SEJ_NBJ IS NULL THEN 0 ELSE T_MCOB.SEJ_NBJ END AS SEJ_NBJ,
							T_MCOB.GRG_GHM
			from %scan(&jnt_mco_pop., 1, "'")
					inner join T_MCO&aa.B T_MCOB on (T_MCOC.ETA_NUM = T_MCOB.ETA_NUM and T_MCOC.RSA_NUM = T_MCOB.RSA_NUM)	
			where %scan(&flt_ghm_m., 1, "'") and      
							(T_MCOC.ETA_NUM not in ('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', '750041543', '750100018',
													'750100042', '750100075', '750010083', '750100091', '750100109', '750100125', '750100166', '750100208',
													'750100216', '750100232', '750100273', '750100299', '750801441', '750803447', '750803454', '910100015',
													'910100023', '920100013', '920100021', '920100039', '920100047', '920100054', '920100062', '930100011',
													'930100037', '930100045', '940100027', '940100035', '940100043', '940100050', '940100068', '950100016',
													'690783154', '690784137', '690784152', '690784178', '690787478', '830100558') 
							and not(T_MCOB.GRG_RET = '024')
							and T_MCOB.GRG_GHM not in ('90H01Z', '90Z00Z', '90Z01Z', '90Z02Z', '90Z03Z')
							and ((T_MCOB.SEJ_TYP = 'A' or T_MCOB.SEJ_TYP is null) or (T_MCOB.SEJ_TYP = 'B' and T_MCOB.GRG_GHM not in ('28Z14Z', '28Z15Z', '28Z16Z'))))
							)
			BY ORACLE;
			quit;

			proc sql;
			create table &lib_out..&tab_out._&aaaa._ghm as 
			select NIR_ANO_17, 
					ETA_NUM, 
					RSA_NUM, 
					datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
					datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
					HSP_OGN, 
					SEJ_NBJ, 
					GRG_GHM
			from orauser.&tab_out._&aaaa._ghm;
			quit;
			%checkDrop(&tab_out._&aaaa._ghm);

	%end;	/* Fermeture de la macro */
%end;/* Fin de la boucle des annees */


	/* Permet de concatener toutes les tables annuelles GHM en une seule table */
	%if &opt_conc. = 1 %then %do;
		data &lib_out..&tab_out._ghm;
		set
		%do annee = %sysfunc(max(&ann_deb., 2005)) %to %sysfunc(min(&ann_fin., &ann_act. - 1));
			&lib_out..&tab_out._&annee._ghm
		%end;;
		run;
	%end; /* Fin de la boucle permettant la concatenation des tables intermediaires */


	/* Permet de supprimer les tables annuelles/intermediaires GHM */
	%if &opt_suppr. = 1 %then %do;
		%do aaaa = %sysfunc(max(&ann_deb., 2005)) %to %sysfunc(min(&ann_fin., &ann_act. - 1));
			proc delete data= &lib_out..&tab_out._&aaaa._ghm;run;
		%end;
	%end; /* Fin de la boucle permettant la suppression des tables intermediaires */


	/* Messages d'informations sur les donnees extraites */
	%if &opt_ghm_inc. = and &opt_ghm_exc. = %then %do;
		%put WARNING: Aucun des 2 parametres indiquant les codes GHM a extraire n ont ete renseignes. Vous avez extrait la totalite des GHM concernes par les autres parametres;
	%end;
	%if &ann_deb. LE 2009 and &ann_fin. GE 2005 %then %do;
		%put WARNING: Les donnees liees aux hospitalisations MCO concernent uniquement le regime general (De 2005 a 2009 inclus);
	%end;
	%if &ann_deb. LE 2015 and &ann_fin. GE 2010 %then %do;
		%put WARNING: Les donnees liees aux hospitalisations MCO concernent les 3 regimes (RG, RA, RSI), de 2010 a 2015 inclus.;
	%end;
	%if &ann_deb. LE &ann_act. and &ann_fin. GE 2016 %then %do;
		%put WARNING: Les donnees liees aux hospitalisations MCO concernent les 3 regimes (RG, RA, RSI) et les 10 SLM (Depuis Septembre 2016).;
	%end;
	%if &ann_deb. LE 2008 or &ann_fin. LE 2008 %then %do;
		%put WARNING: Les dates d hospitalisations MCO ont ete approximees. Ne disposant que du mois et annee de sortie, nous avons consid�ere une entree au 1er du mois, et une duree de sejour egale a la valeur SEJ_NBJ.;
	%end; /* Fin des messages d'informations */

	%Stop_macro:
	/* Fin du programme */
%mend;
