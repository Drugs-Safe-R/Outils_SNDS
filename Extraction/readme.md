# Macros

Version du 6 octobre 2023

Les macros rendues disponibles sont sous licence Apache 2.0.

Les outils macros mis à disposition par DRUGS-SAFEr sont indépendantes les unes des autres. Elles facilitent l'extraction des médicaments délivrées en ville, des séjours hospitaliers, des ALD, etc. Elles sont décrites ci-dessous :

Avant tout, les macros fournies utilisent une autre macro indispensable **[%multi\_like](%multi_like.sas)**. Ce programme permet de générer des requêtes %like à la suite et ainsi permettre un gain de temps. Les options _opt\_xxx\_inc, opt\_xxx\_exc, opt\_cip, opt\_ald_ font appel à cette macro. Ces options doivent être remplies en séparant chaque code par un espace.

[**%xtr\_pha\_dcir**](%xtr_pha_dcir.sas) permet d'extraire, dans les données du DCIR et durant une période définie par deux dates de début (_dte\_dbt\_prs_) et de fin d'extraction (_dte\_fin\_prs_), les médicaments délivrés identifiés à partir des codes ATC, CIP (_opt\_atc\_inc ; opt\_atc\_exc ; opt\_cip_). Une population (_opt\_pop_) peut être jointée à cette requête pour n'extraire que les médicaments délivrés pour les sujets de cette population (La table doit contenir les identifiants BEN\_NIR\_PSA). En parallèle du référentiel présent sur le portail avec la table IR\_PHA\_R, vous pouvez utiliser votre propre référentiel Médicaments grâce à l'option (_opt\_ref_) où les noms de variables doivent correspondre. Plusieurs autres options sont disponibles pour extraire les libellés (_opt\_xtr\_lib_), les doses et unités (_opt\_xtr\_dos_), ou encore les données des prescripteurs (_opt\_xtr\_psp_) et des exécutants (_opt\_xtr\_pse_).

[**%xtr\_ucd\_dcir**](%xtr_ucd_dcir.sas) est une macro construite de la même manière que celle précédente, elle se consacre uniquement à l'extraction des médicaments délivrés en sus et/ou rétrocédés.

[**%xtr\_hsp\_dcir**](%xtr_hsp_dcir.sas) est un programme d'extraction des données de séjours hospitaliers identifiés à partir des codes CIM-10 (_opt\_cim\_inc ; opt\_cim\_exc_) sur une période donnée par année (_ann\_deb ; ann\_fin_). Une population (_opt\_pop_) peut être jointée à cette requête pour n'extraire que les médicaments délivrés pour les sujets de cette population (La table doit contenir les identifiants BEN\_NIR\_PSA). Plusieurs spécificités existent notamment la particularité d'extraire selon le type d'hospitalisation MCO, HAD, PSY ou SSR (_mod\_hsp_) et selon les codes CIM-10 des diagnostics reliés et associés en plus du diagnostic principal (_opt\_cim\_dgn\_ass_).

[**%xtr\_ald\_dcir**](%xtr_ald_dcir.sas) permet d'extraire les ALD en cours, durant une période de temps donnée par année (_ann\_deb ; ann\_fin_). Cette extraction peut se faire pour une population spécifique (définie par _opt\_pop_) où la table doit contenir la variable BEN\_NIR\_PSA. L'extraction se fait selon des codes CIM-10 à extraire (_opt\_cim\_inc_) ou à exlcure (_opt\_cim\_exc_). Elle peut aussi permettre d'extraire les données selon les grandes catégories d'ALD (_opt\_ald_).

[**%xtr\_cam\_dcir**](%xtr_cam_dcir.sas) permet d'extraire, dans les données du SNDS et durant une période de temps donné (_ann\_deb ; ann\_fin_), les actes médicaux réalisés identifiés à partir des codes CCAM à inclure (_opt\_cam\_inc_) ou à exclure (_opt\_cam\_exc_). Comme précédemment, une population (_opt\_pop_) peut être jointée à cette requête pour n'extraire que les actes médicaux réalisés pour les sujets de cette population (la table doit contenir les identifiants BEN\_NIR\_PSA).

[**%xtr\_lpp\_dcir**](%xtr_lpp_dcir.sas) est construite comme les précédentes, elle permet d'extraire les données de délivrances des dispositifs médicaux identifiés par les codes à inclure (_opt\_lpp\_inc_) et/ou à exclure (_opt\_lpp\_exc_)